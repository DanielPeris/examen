# frozen_string_literal: true

str = SecurityTestRepository.where(host: NORAD_REGISTRY).first_or_initialize
str.public = true
str.official = true
str.name = 'Norad'
str.allow_official_changes = true
str.save!

sc = SecurityContainer.where(name: 'configurable-test:0.0.1', security_test_repository_id: str.id).first_or_initialize
sc.prog_args = '%{target}:%{port} --flag=%{configurable_option}'
sc.default_config = { configurable_option: 'default-value' }
sc.category = :blackbox
sc.test_types = ['ssl_crypto']
sc.configurable = true
sc.help_url = 'https://norad.gitlab.io/docs/#configurable-test'
sc.save!
