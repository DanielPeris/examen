# frozen_string_literal: true

# I'm disabling these cops here because I think in a script scenario it makes sense to sometimes have
# large methods and more than 1 empty line between sections of code.
#
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
# rubocop:disable Layout/EmptyLines
# rubocop:disable Rails/Output


# Create an organization with lots of results/assessments/machines for performance testing

USER_UID = 'perf-user'
NUM_MACHINES = 100
NUM_SECURITY_CONTAINERS = 10
NUM_ORG_SCANS = 500
NUM_MACHINE_SCANS = 50
NUM_ASSESSMENTS_PER_SCAN = 100
NUM_CONTAINERS_PER_SCAN = 5
NUM_RESULTS_PER_ASSESSMENT = 10
ASSESSMENT_TYPE_PROC = proc { %i[white_box_assessment black_box_assessment].sample }
RESULT_STATUS_PROC = proc { Result.statuses.keys.sample }
RESULT_SIR_PROC = proc { Result.sirs.keys.sample }

# Maximum number of records to build and import at one time
# A sensible size is 20_000 for machines with at least 1 GB RAM
BATCH_SIZE = 20_000



# ##############################################################################
# BEGIN Helpers
# ##############################################################################
def random_machine_id
  @machine_ids ||= Machine.pluck(:id)
  @machine_ids.sample
end

def random_container_id(num = nil)
  @container_ids ||= SecurityContainer.pluck(:id)
  num ? @container_ids.sample(num) : @container_ids.sample
end

def repo
  str = SecurityTestRepository.where(host: NORAD_REGISTRY).first_or_initialize
  str.public = true
  str.official = true
  str.name = 'Norad'
  str.allow_official_changes = true
  str.save!
  str
end

def clean!
  puts 'Removing existing test data...'
  User.find_by(uid: USER_UID)&.destroy!
  Organization.find_by(uid: "#{USER_UID}-org-default")&.destroy!
end

def create_user_and_org
  puts
  puts 'Creating User and Org...'
  user = FactoryBot.create(:user_with_sso, uid: USER_UID)
  @org = user.organizations.first
end

def create_security_containers
  puts
  puts 'Creating Security Containers...'
  (NUM_SECURITY_CONTAINERS / 2).times do
    FactoryBot.create(:security_container, security_test_repository: repo)
    FactoryBot.create(:whitebox_container, security_test_repository: repo)
  end
end

def create_machines
  puts
  puts "Creating #{NUM_MACHINES} Machines..."
  (1..NUM_MACHINES).each_slice(BATCH_SIZE) do |slice|
    puts "Building #{slice.size} Machines..."
    machines = FactoryBot.attributes_for_list(:machine, slice.size, organization_id: @org.id)
    puts "Importing #{slice.size} Machines..."
    Machine.import machines, validate: false
  end
  puts "#{NUM_MACHINES} / #{NUM_MACHINES}"
end

def create_org_scans
  puts
  puts "Creating #{NUM_ORG_SCANS} Org Scans..."
  (1..NUM_ORG_SCANS).each_slice(BATCH_SIZE) do |slice|
    puts "Building #{slice.size} Org Scans..."
    dcs = []
    slice.each do |i|
      $stdout.flush
      print "#{i} / #{NUM_ORG_SCANS}\r"
      dcs <<
        FactoryBot.attributes_for(:docker_command,
                                  machine_id: nil,
                                  organization_id: @org.id)
    end
    puts
    puts "Importing #{slice.size} Org Scans..."
    DockerCommand.import(dcs, validate: false)
  end
end

def create_machine_scans
  puts
  puts "Creating #{NUM_MACHINE_SCANS} Machine Scans..."
  (1..NUM_MACHINE_SCANS).each_slice(BATCH_SIZE) do |slice|
    puts "Building #{slice.size} Machine Scans..."
    dcs = []
    slice.each do |i|
      $stdout.flush
      print "#{i} / #{NUM_MACHINE_SCANS}\r"
      dcs << FactoryBot.attributes_for(
        :docker_command,
        machine_id: random_machine_id,
        organization_id: nil
      )
    end
    puts
    puts "Importing #{slice.size} Machine Scans..."
    DockerCommand.import(dcs, validate: false)
  end
end

def join_scans_to_containers
  puts
  puts 'Joining Docker Commands to Containers...'
  dc_ids = DockerCommand.pluck(:id)
  scs = []
  dc_ids.each do |dc_id|
    container_ids = random_container_id(NUM_CONTAINERS_PER_SCAN)
    container_ids.each do |c_id|
      scs << {
        docker_command_id: dc_id,
        security_container_id: c_id
      }
    end
  end
  ScanContainer.import(scs, validate: false)
  puts "Created #{ScanContainer.count} ScanContainers."
end

def create_assessments
  puts
  puts "Creating #{NUM_ASSESSMENTS_PER_SCAN * DockerCommand.count} Assessments..."
  dc_ids = DockerCommand.pluck(:id)
  dc_ids.each_slice(BATCH_SIZE / NUM_ASSESSMENTS_PER_SCAN) do |slice|
    asses = []
    slice.each.with_index do |dc_id, i|
      NUM_ASSESSMENTS_PER_SCAN.times do |j|
        total = NUM_ASSESSMENTS_PER_SCAN * slice.size
        completed = NUM_ASSESSMENTS_PER_SCAN * i + j + 1
        $stdout.flush
        print "Building #{format('%05d', completed)} / #{format('%05d', total)} Assessments...\r"
        asses << FactoryBot.attributes_for(
          ASSESSMENT_TYPE_PROC.call,
          machine_id: random_machine_id,
          security_container_id: random_container_id,
          docker_command_id: dc_id,
          service: nil,
          security_container_secret: nil,
          state_transition_time: 'now()'
        )
      end
    end
    puts
    puts "Importing #{NUM_ASSESSMENTS_PER_SCAN * slice.size} Assessments..."
    Assessment.import(asses, validate: false)
    puts "Created #{Assessment.count} / #{NUM_ASSESSMENTS_PER_SCAN * DockerCommand.count} Assessments."
  end
end

def create_results
  puts
  puts "Creating #{NUM_RESULTS_PER_ASSESSMENT * Assessment.count} Results..."
  ass_ids = Assessment.pluck(:id)
  ass_ids.each_slice(BATCH_SIZE / NUM_RESULTS_PER_ASSESSMENT) do |slice|
    results = []
    slice.each.with_index do |ass_id, i|
      NUM_RESULTS_PER_ASSESSMENT.times do |j|
        total = NUM_RESULTS_PER_ASSESSMENT * slice.size
        completed = NUM_RESULTS_PER_ASSESSMENT * i + j + 1
        $stdout.flush
        print "Building #{format('%06d', completed)} / #{format('%06d', total)} Results...\r"
        sir = RESULT_SIR_PROC.call
        status = RESULT_STATUS_PROC.call

        # FactoryBot doesn't build "{column}_encrypted" so activerecord-import thinks they're blank, causing an error
        # So we need to build an actual Result object here (or set _encrypted columns to dummy data)
        results << FactoryBot.build(
          :result,
          sir: sir,
          status: status,
          assessment_id: ass_id
        )
      end
    end
    puts
    puts "Importing #{NUM_RESULTS_PER_ASSESSMENT * slice.size} Results..."
    Result.import(results, validate: false)
    puts "Created #{Result.count} / #{NUM_RESULTS_PER_ASSESSMENT * Assessment.count} Results."
  end
end
# ##############################################################################
# END Helpers
# ##############################################################################



# ##############################################################################
# BEGIN Main
# ##############################################################################

# Start by removing existing performance seed data
clean!

# Create User and Org
create_user_and_org

# Create Security Containers
create_security_containers

# Create Machines
create_machines

# Create Org Scans
create_org_scans

# Create Machine Scans
create_machine_scans

# Associate scans to containers
join_scans_to_containers

# Create Assessments
create_assessments

# Create Results
create_results

# ##############################################################################
# END Main
# ##############################################################################


# rubocop:enable Metrics/AbcSize
# rubocop:enable Metrics/MethodLength
# rubocop:enable Layout/EmptyLines
# rubocop:enable Rails/Output
