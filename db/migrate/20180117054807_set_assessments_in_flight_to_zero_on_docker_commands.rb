class SetAssessmentsInFlightToZeroOnDockerCommands < ActiveRecord::Migration[5.0]
  def up
    execute <<~SQL
      UPDATE docker_commands
      SET assessments_in_flight = 0
      WHERE assessments_in_flight < 0
    SQL
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
