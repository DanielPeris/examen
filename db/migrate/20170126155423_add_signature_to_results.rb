class AddSignatureToResults < ActiveRecord::Migration[5.0]
  def change
    add_column :results, :signature, :string
    add_index :results, :signature
    reversible { |direction| direction.up { initialize_signatures } }
    change_column_null :results, :signature, false
  end

  private

  # Postgres has no SHA256 built-in, so use two MD5's for initialization instead
  def initialize_signatures
    execute <<~SQL
      UPDATE results
      SET signature = ( md5( random()::text ) || md5( random()::text ) );
    SQL
  end
end
