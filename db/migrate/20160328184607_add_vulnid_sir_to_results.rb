class AddVulnidSirToResults < ActiveRecord::Migration[4.2]
  def change
    add_column :results, :nid, :string, null: false
    add_column :results, :sir, :integer, null: false, default: 0
  end
end
