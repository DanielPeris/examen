class UpdateForeignKeyConstraintForSecurityContainerConfigs < ActiveRecord::Migration[4.2]
  def up
    remove_foreign_key :security_container_configs, :security_containers
    add_foreign_key :security_container_configs, :security_containers, on_delete: :cascade
  end

  def down
    add_foreign_key :security_container_configs, :security_containers
  end
end
