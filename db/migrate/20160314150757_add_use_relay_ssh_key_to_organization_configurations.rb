class AddUseRelaySshKeyToOrganizationConfigurations < ActiveRecord::Migration[4.2]
  def change
    add_column :organization_configurations, :use_relay_ssh_key, :boolean, default: false, null: false
  end
end
