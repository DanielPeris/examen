class AddKeySignatureToDockerRelays < ActiveRecord::Migration[4.2]
  def up
    add_column :docker_relays, :key_signature, :string
    DockerRelay.all.each do |relay|
      relay.send(:generate_key_signature)
      relay.save!
    end
  end

  def down
    remove_column :docker_relays, :key_signature
  end
end
