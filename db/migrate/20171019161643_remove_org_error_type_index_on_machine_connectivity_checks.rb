class RemoveOrgErrorTypeIndexOnMachineConnectivityChecks < ActiveRecord::Migration[5.0]
  disable_ddl_transaction!

  def up
    remove_index :machine_connectivity_checks, %i[machine_id organization_error_type]
  end

  def down
    add_index :machine_connectivity_checks, %i[machine_id organization_error_type],
      name: :index_machine_connectivity_checks_on_m_id_and_o_e_type, algorithm: :concurrently
  end
end
