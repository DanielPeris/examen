class AddNullContraintForKeySignatureToMachineAgents < ActiveRecord::Migration[4.2]
  def change
    change_column_null :machine_agents, :key_signature, false
  end
end
