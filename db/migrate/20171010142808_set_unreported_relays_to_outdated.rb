class SetUnreportedRelaysToOutdated < ActiveRecord::Migration[5.0]
  def up
    execute up_sql
  end

  def down
    execute down_sql
  end

  private

  def up_sql
    <<~SQL
      UPDATE docker_relays
      SET outdated = 't'
      WHERE last_reported_version IS NULL;
    SQL
  end

  def down_sql
    <<~SQL
      UPDATE docker_relays
      SET outdated = 'f'
      WHERE last_reported_version IS NULL;
    SQL
  end
end
