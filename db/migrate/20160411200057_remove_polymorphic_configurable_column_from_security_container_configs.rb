class RemovePolymorphicConfigurableColumnFromSecurityContainerConfigs < ActiveRecord::Migration[4.2]
  class SecurityContainerConfig < ActiveRecord::Base
    belongs_to :configurable, polymorphic: true
    belongs_to :machine
    belongs_to :organization
  end

  def up
    change_table :security_container_configs do |t|
      t.references :machine, index: true
      t.references :organization, index: true
    end
    add_foreign_key :security_container_configs, :machines, on_delete: :cascade
    add_foreign_key :security_container_configs, :organizations, on_delete: :cascade
    SecurityContainerConfig.reset_column_information

    SecurityContainerConfig.all.each do |sc|
      if sc.configurable_type == 'Organization'
        sc.organization_id = sc.configurable_id
      else
        sc.machine_id = sc.configurable_id
      end
      sc.save!
    end

    change_table :security_container_configs do |t|
      t.remove :configurable_type
      t.remove :configurable_id
    end
  end

  def down
    change_table :security_container_configs do |t|
      t.references :configurable, polymorphic: true, index: true
    end
    SecurityContainerConfig.reset_column_information

    SecurityContainerConfig.all.each do |sc|
      if sc.machine
        sc.configurable = sc.machine
      elsif sc.organization
        sc.configurable = sc.organization
      end
      sc.save!
    end

    change_table :security_container_configs do |t|
      t.remove :machine_id
      t.remove :organization_id
    end
  end
end
