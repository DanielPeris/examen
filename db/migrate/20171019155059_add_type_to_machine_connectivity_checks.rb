class AddTypeToMachineConnectivityChecks < ActiveRecord::Migration[5.0]
  def up
    add_column :machine_connectivity_checks, :type, :string
    execute set_type_sql('UnableToPingMachineError', 'PingConnectivityCheck')
    execute set_type_sql('UnableToSshToMachineError', 'SshConnectivityCheck')
    change_column_null :machine_connectivity_checks, :type, false
  end

  def down
    remove_column :machine_connectivity_checks, :type
  end

  private

  def set_type_sql(org_error_type, type)
    <<~SQL
      UPDATE machine_connectivity_checks
      SET type = '#{type}'
      WHERE organization_error_type = '#{org_error_type}';
    SQL
  end
end
