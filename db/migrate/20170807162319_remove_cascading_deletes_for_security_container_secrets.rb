class RemoveCascadingDeletesForSecurityContainerSecrets < ActiveRecord::Migration[5.0]
  # Remove the cascading deletes on all security container secret children because we now
  # remove secrets older than 24 hours and don't want it to remove other data.
  def change
    remove_foreign_key :service_discoveries, :security_container_secrets
    add_foreign_key :service_discoveries, :security_container_secrets, column: :container_secret_id, on_delete: :nullify

    remove_foreign_key :assessments, :security_container_secrets
    add_foreign_key :assessments, :security_container_secrets, on_delete: :nullify

    change_column_null :machine_connectivity_checks, :security_container_secret_id, true
    remove_foreign_key :machine_connectivity_checks, :security_container_secrets
    add_foreign_key :machine_connectivity_checks, :security_container_secrets, on_delete: :nullify
  end
end
