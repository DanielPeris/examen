# frozen_string_literal: true
class EnableVaultForSshConfig < ActiveRecord::Migration[4.2]
  # I'm updating an old migration because anything that tries to build the database from scratch
  # will fail unless we configure the SshConfig class to use vault. At the time of this comment
  # being written, the SshConfig model has been removed, so we need to stub it here.
  class SshConfig < ActiveRecord::Base
    # Vault Stuff
    include Vault::EncryptedModel
    vault_attribute :key
    vault_attribute :username
  end

  def up
    add_column :ssh_configs, :key_encrypted, :string
    add_column :ssh_configs, :username_encrypted, :string
    SshConfig.reset_column_information

    # We've gotta access the existing AR attribute directly since Vault is now defining the #key and
    # #username methods
    SshConfig.all.each do |config|
      config.key = config.attributes['key']
      config.username = config.attributes['username']
      config.save!
    end

    remove_column :ssh_configs, :key
    remove_column :ssh_configs, :username
  end

  def down
    remove_column :ssh_configs, :key_encrypted
    remove_column :ssh_configs, :username_encrypted
    add_column :ssh_configs, :key, :string
    add_column :ssh_configs, :username, :string
  end
end
