class CreateRequirements < ActiveRecord::Migration[4.2]
  def change
    create_table :requirements do |t|
      t.string :name
      t.string :description
      t.references :requirement_group, index: true, null: false

      t.timestamps null: false
    end
    add_foreign_key :requirements, :requirement_groups, on_delete: :cascade
  end
end
