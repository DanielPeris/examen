# frozen_string_literal: true

class UpdateSignatureOnErroredResults < ActiveRecord::Migration[5.0]
  # See app/services/result_generator.rb in this commit and the one before it for how these were generated
  OLD_TIMEDOUT_SIGNATURE = '1c4365d5afb7f6fd95740c6321150cdadf996c99fb094dbb977b850b83243221'
  NEW_TIMEDOUT_SIGNATURE = 'd37402715d3017c0a8a05bd7f54cb8ffd0a98996bb0efaac95201d4de35e5c1c'
  OLD_ERRORED_SIGNATURE = '3bf184b1d5a1a7ba78912fb002fb45e4a8cd4bd0364d7bf827f8b8333e6baa28'
  NEW_ERRORED_SIGNATURE = 'c8ef54a3e8268ddc8ddacaaafdf35a081bfb7494a9a05c6cea18c90150b6b1fc'

  def up
    execute update_old_sig_to_new_sig_sql(OLD_TIMEDOUT_SIGNATURE, NEW_TIMEDOUT_SIGNATURE)
    execute update_old_sig_to_new_sig_sql(OLD_ERRORED_SIGNATURE, NEW_ERRORED_SIGNATURE)
  end

  def down
    execute update_old_sig_to_new_sig_sql(NEW_TIMEDOUT_SIGNATURE, OLD_TIMEDOUT_SIGNATURE)
    execute update_old_sig_to_new_sig_sql(NEW_ERRORED_SIGNATURE, OLD_ERRORED_SIGNATURE)
  end

  private

  def update_old_sig_to_new_sig_sql(current_sig, replacement_sig)
    <<~SQL
      UPDATE results
      SET signature = '#{replacement_sig}'
      WHERE signature = '#{current_sig}';
    SQL
  end
end
