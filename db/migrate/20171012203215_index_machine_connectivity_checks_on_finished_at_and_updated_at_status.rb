class IndexMachineConnectivityChecksOnFinishedAtAndUpdatedAtStatus < ActiveRecord::Migration[5.0]
  disable_ddl_transaction!

  def change
    # Create the index concurrently
    add_index :machine_connectivity_checks, :updated_at, algorithm: :concurrently
    add_index :machine_connectivity_checks, :finished_at, algorithm: :concurrently
    add_index :machine_connectivity_checks, :status, algorithm: :concurrently
  end
end
