# frozen_string_literal: true

# set path to application
app_dir = File.expand_path('../..', __FILE__)
working_directory app_dir

# Set unicorn options
worker_processes ENV['WORKER_PROCESSES'].to_i
preload_app true
timeout 30

# Enable this flag to have unicorn test client connections by writing the
# # beginning of the HTTP headers before calling the application. This
# # prevents calling the application for connections that have disconnected
# # while queued. This is only guaranteed to detect clients on the same
# # host unicorn runs on, and unlikely to detect disconnects even on a
# # fast LAN.
check_client_connection false

# Set up socket location
listen ENV['LISTEN_ON']

# Set master PID location
pid "#{app_dir}/tmp/api.pid"
