# frozen_string_literal: true

NORAD_REGISTRY = "#{ENV.fetch('REGISTRY_SERVER')}:#{ENV.fetch('REGISTRY_PORT')}"
