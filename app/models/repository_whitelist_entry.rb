# frozen_string_literal: true

# == Schema Information
#
# Table name: repository_whitelist_entries
#
#  id                          :integer          not null, primary key
#  organization_id             :integer          not null
#  security_test_repository_id :integer          not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_r_w_entries_on_o_id_and_s_t_r_id  (organization_id,security_test_repository_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_6d4fcfc98d  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#  fk_rails_daf54902a6  (organization_id => organizations.id) ON DELETE => cascade
#

# Organizations can't use private test repositories until they have been "whitelisted" by the repository admin.
# This class and DB table represents that whitelist.
class RepositoryWhitelistEntry < ApplicationRecord
  # Prevent adding / modifying Official Repo to whitelist
  include RepositoryImmutable

  # RBAC
  include Authority::Abilities
  resourcify

  # Error Watching
  include OrganizationErrorWatcher
  watch_for_organization_errors RepositoryWithoutRelayError

  # Associations
  belongs_to :organization, inverse_of: :repository_whitelist_entries
  belongs_to :security_test_repository, inverse_of: :repository_whitelist_entries
  validate :prevent_official_changes
  validate :repository_hosts_unique_per_organization

  # Delegations
  delegate :official, :official_changed?, to: :security_test_repository

  private

  # Validations

  # With this DB structure it doesn't make sense to have duplicate repository hosts per organization,
  # as we use the repository host, organization whitelist, and container name to uniquely identify
  # containers for docker commands. In the future we may want to add a SecurityTestRepositoryConfig model
  # to achieve having different sets of containers for identical hosts whitelisted for the same organization.
  def repository_hosts_unique_per_organization
    if organization.whitelisted_repositories.where('host IN (?)', security_test_repository.host.to_s).exists?
      errors.add(:base, 'A test repository with that host is already whitelisted')
      throw :abort
    end
    true
  end
end
