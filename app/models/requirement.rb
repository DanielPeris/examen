# frozen_string_literal: true

# == Schema Information
#
# Table name: requirements
#
#  id                   :integer          not null, primary key
#  name                 :string
#  description          :string
#  requirement_group_id :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_requirements_on_requirement_group_id  (requirement_group_id)
#
# Foreign Keys
#
#  fk_rails_cce12a8273  (requirement_group_id => requirement_groups.id) ON DELETE => cascade
#

class Requirement < ApplicationRecord
  include Authority::Abilities

  belongs_to :requirement_group, inverse_of: :requirements
  has_many :provisions, inverse_of: :requirement
  has_many :security_containers, through: :provisions, inverse_of: :requirements
end
