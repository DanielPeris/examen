# frozen_string_literal: true

class MachineScanSummary < ScanSummary
  attr_accessor :machine

  def query
    MachineScanSummaryQuery.new(relation: machine)
  end
end
