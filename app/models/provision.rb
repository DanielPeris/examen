# frozen_string_literal: true

# == Schema Information
#
# Table name: provisions
#
#  id                    :integer          not null, primary key
#  security_container_id :integer          not null
#  requirement_id        :integer          not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
# Indexes
#
#  index_provisions_on_requirement_id         (requirement_id)
#  index_provisions_on_security_container_id  (security_container_id)
#
# Foreign Keys
#
#  fk_rails_6687cc6c26  (requirement_id => requirements.id) ON DELETE => cascade
#  fk_rails_7351aeb5f9  (security_container_id => security_containers.id) ON DELETE => cascade
#

class Provision < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Associations
  belongs_to :security_container, inverse_of: :provisions
  belongs_to :requirement, inverse_of: :provisions
end
