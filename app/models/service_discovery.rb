# frozen_string_literal: true

# == Schema Information
#
# Table name: service_discoveries
#
#  id                  :integer          not null, primary key
#  machine_id          :integer
#  container_secret_id :integer
#  state               :integer
#  error_message       :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_service_discoveries_on_container_secret_id  (container_secret_id)
#  index_service_discoveries_on_in_progress_state    (machine_id,state) UNIQUE WHERE (state = 1)
#  index_service_discoveries_on_machine_id           (machine_id)
#  index_service_discoveries_on_pending_state        (machine_id,state) UNIQUE WHERE (state = 0)
#
# Foreign Keys
#
#  fk_rails_92a60df35e  (container_secret_id => security_container_secrets.id) ON DELETE => nullify
#  fk_rails_fc86baf86f  (machine_id => machines.id) ON DELETE => cascade
#

class ServiceDiscovery < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # State Machine
  include ServiceDiscoveryStateMachine

  # Validations
  validates :machine, presence: true
  validates :container_secret, presence: true
  validates :error_message, absence: true, unless: 'failed?'
  validates :error_message, presence: true, if: 'failed?'

  # Associations
  belongs_to :machine
  belongs_to :container_secret, class_name: 'SecurityContainerSecret'

  # Callbacks
  after_commit :schedule_discovery, on: :create
  before_validation on: :create do
    build_container_secret
  end

  scope :latest_10, -> { order(created_at: :desc).limit(10) }

  def self.scheduled
    pending.first || in_progress.first
  end

  def self.current
    in_progress.first
  end

  def self.stale
    in_progress.where('updated_at <= ?', 12.hours.ago)
  end

  # Provide a shortcut to the organization
  delegate :organization, to: :machine

  def shared_secret
    container_secret&.secret
  end

  private

  def schedule_discovery
    ServiceDiscoveryJob.perform_later self
  end
end
