# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

require './lib/norad_relay_helpers'

class MachineError < OrganizationError
  include MachineAsErrable

  attr_accessor :machine_connectivity_check

  before_destroy :check_is_latest
  before_save :check_is_latest

  class << self
    include NoradRelayHelpers

    def check(org, options = {})
      subject = options.fetch(:subject)
      subjects = Array(subject)
      machines = subjects_are_machines?(subjects) ? subjects : org.machines
      queue_containers(machines, relay_opts(org))
    end

    private

    def queue_containers(machines, relay_opts = {})
      machines.each do |machine|
        next if machine.destroyed?
        perform_check_for_machine(machine, relay_opts)
      end
    end

    def perform_check_for_machine(machine, relay_opts)
      check = create_check(machine)
    rescue ActiveRecord::RecordInvalid
      false
    else
      ScheduleMachineConnectivityCheckJob.perform_later(check.id, relay_opts)
    end

    def create_check(machine)
      relation_name = name.constantize::CHECK_KLASS.underscore.pluralize.to_sym
      check = machine.send(relation_name).build
      check.build_security_container_secret
      check.save!
      check
    end

    def subjects_are_machines?(subjects)
      subjects.any? && subjects.all? { |subject| subject.is_a? Machine }
    end
  end

  private

  def check_is_latest
    return if !machine_connectivity_check || machine_connectivity_check.latest?
    errors.add(:base, 'Aborting: newer machine connectivity check was found.')
    throw :abort
  end
end
