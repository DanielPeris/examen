# frozen_string_literal: true

# == Schema Information
#
# Table name: docker_relays
#
#  id                            :integer          not null, primary key
#  organization_id               :integer          not null
#  public_key                    :text             not null
#  queue_name                    :string           not null
#  state                         :integer          default("online"), not null
#  last_heartbeat                :datetime         not null
#  verified                      :boolean          default(FALSE), not null
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  key_signature                 :string           not null
#  file_encryption_key_encrypted :string
#  last_reported_version         :string
#  outdated                      :boolean          default(FALSE), not null
#
# Indexes
#
#  index_docker_relays_on_organization_id  (organization_id)
#  index_docker_relays_on_outdated         (outdated)
#  index_docker_relays_on_queue_name       (queue_name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_b84167028c  (organization_id => organizations.id) ON DELETE => cascade
#

class DockerRelay < ApplicationRecord
  include ActsAsAgent
  FILE_ENCRYPTION_KEY_LENGTH = 4096
  VERSION_FORMAT = /\A\d+\.\d+\.\d+\z/

  # Vault
  include Vault::EncryptedModel
  vault_attribute :file_encryption_key

  # RBAC
  include Authority::Abilities

  # State Machine
  include DockerRelayStateMachine

  # Attribute Information
  attr_readonly :organization_id

  # Validations
  validates :organization, presence: true
  validates :file_encryption_key, presence: true
  validate :file_encryption_key_length
  validates :last_reported_version,
            format: { with: VERSION_FORMAT, message: 'must take the form 1.2.3' },
            allow_nil: true

  # Associations
  belongs_to :organization, inverse_of: :docker_relays

  # Callback Declaration
  before_create do
    self.verified = true if organization.configuration.auto_approve_docker_relays
    true
  end
  before_validation :generate_file_encryption_key
  before_validation :determine_if_outdated

  include OrganizationErrorWatcher
  watch_for_organization_errors UnreachableMachineError, RelayError, RepositoryWithoutRelayError
  watch_for_organization_errors UnableToPingMachineError, UnableToSshToMachineError,
                                if: :run_connectivity_checks?,
                                attributes: %i[verified state]

  def file_encryption_key_to_rsa
    OpenSSL::PKey::RSA.new(Base64.strict_decode64(file_encryption_key))
  end

  def process_heartbeat
    transaction do
      self.last_heartbeat = Time.current
      go_online if offline?
      save!
      setup_queue
    end
  end

  private

  def determine_if_outdated
    latest_version = RelayImageInfoFetcher.cached_latest_version
    return unless last_reported_version && latest_version
    self.outdated = Gem::Version.new(last_reported_version) < Gem::Version.new(latest_version)

    # Prevent deprecation warnings
    nil
  end

  def run_connectivity_checks?
    verified && online? && (created? || destroyed? || updated_with_relevant_connectivity_changes?)
  end

  def updated_with_relevant_connectivity_changes?
    updated? && (state_changed_and_has_errors? || verified_changed?)
  end

  def created?
    transaction_include_any_action?(%i[create])
  end

  def updated?
    transaction_include_any_action?(%i[update])
  end

  def organization_has_connectivity_errors?
    organization.organization_errors.where(type: %w[UnableToSshToMachineError UnableToPingMachineError]).exists?
  end

  def associated_organization_token
    organization.token
  end

  def state_changed_and_has_errors?
    state_changed? && organization_has_connectivity_errors?
  end

  def generate_file_encryption_key
    file_encryption_key_to_rsa
  rescue OpenSSL::PKey::RSAError, NoMethodError
    self.file_encryption_key = Base64.strict_encode64(OpenSSL::PKey::RSA.new(FILE_ENCRYPTION_KEY_LENGTH).to_pem)
  end

  def file_encryption_key_length
    add_file_encryption_key_length_error unless file_encryption_key_to_rsa.n.num_bits == FILE_ENCRYPTION_KEY_LENGTH
  rescue OpenSSL::PKey::RSAError, NoMethodError
    add_file_encryption_key_length_error
  end

  def add_file_encryption_key_length_error
    errors.add :file_encryption_key, "must be an RSA key of length #{FILE_ENCRYPTION_KEY_LENGTH}"
  end
end
