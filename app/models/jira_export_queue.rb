# frozen_string_literal: true

# == Schema Information
#
# Table name: result_export_queues
#
#  id              :integer          not null, primary key
#  organization_id :integer          not null
#  created_by      :string           not null
#  type            :string           not null
#  auto_sync       :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_result_export_queues_on_organization_id           (organization_id)
#  index_result_export_queues_on_organization_id_and_type  (organization_id,type) UNIQUE WHERE ((type)::text = 'InfosecExportQueue'::text)
#  index_result_export_queues_on_type                      (type)
#
# Foreign Keys
#
#  fk_rails_e5fc28c9ec  (organization_id => organizations.id) ON DELETE => cascade
#

class JiraExportQueue < ResultExportQueue
  # RBAC
  self.authorizer_name = "#{superclass.name}Authorizer"

  has_one :custom_jira_configuration, foreign_key: :result_export_queue_id, inverse_of: :jira_export_queue
  validates :custom_jira_configuration, presence: true
  accepts_nested_attributes_for :custom_jira_configuration # destroy handled at the db level

  def target_connection_settings
    { target: 'jira', settings: custom_jira_configuration.attributes }
  end
end
