# frozen_string_literal: true

# This is a specific instance of a template following the template design
# pattern. See the base class ResultAbstractErrorAttributes for details.

class ResultUnexpectedErrorAttributes < ResultAbstractErrorAttributes
  private

  def generate_title
    'Unexpected Error'
  end

  def generate_description(exception)
    "The security test encountered an unexpected error: #{exception.message}"
  end

  def generate_nid
    'unexpected-error:10010'
  end
end
