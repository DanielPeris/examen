# frozen_string_literal: true

class OrganizationScanSummary < ScanSummary
  attr_accessor :organization

  def query
    OrganizationScanSummaryQuery.new(relation: organization)
  end
end
