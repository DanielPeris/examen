# frozen_string_literal: true

# == Schema Information
#
# Table name: security_container_configs
#
#  id                             :integer          not null, primary key
#  security_container_id          :integer
#  enabled_outside_of_requirement :boolean          default(FALSE), not null
#  machine_id                     :integer
#  organization_id                :integer
#  values_encrypted               :string
#
# Indexes
#
#  index_security_container_configs_on_machine_id             (machine_id)
#  index_security_container_configs_on_organization_id        (organization_id)
#  index_security_container_configs_on_security_container_id  (security_container_id)
#
# Foreign Keys
#
#  fk_rails_191d669b7b  (machine_id => machines.id) ON DELETE => cascade
#  fk_rails_7f035feecf  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_a879277af4  (security_container_id => security_containers.id) ON DELETE => cascade
#

class SecurityContainerConfig < ApplicationRecord
  # Vault Stuff
  include Vault::EncryptedModel
  vault_attribute :values, serialize: :json

  # RBAC
  include Authority::Abilities

  # Attribute Information
  attr_readonly :security_container_id, :machine_id, :organization_id
  EMPTY_CONFIG_VALUE = '~!_NORAD_EMPTY_CONFIG_VALUE_!~'

  # Associations
  acts_as_poly :configurable, :machine, :organization, inverse_of: :security_container_configs
  belongs_to :security_container, inverse_of: :security_container_configs

  # Validations
  validates :configurable, presence: true
  validates :security_container, presence: true
  validates :machine, presence: true, if: proc { |a| a.organization.blank? }
  validates :organization, presence: true, if: proc { |a| a.machine.blank? }
  validate :values_contains_all_arguments

  # Callback Declarations
  before_validation :initialize_config_values

  include OrganizationErrorWatcher
  watch_for_organization_errors(
    NoSshKeyPairError, NoSshKeyPairAssignmentError, organization_method: :resolved_organization
  )

  scope(
    :explicitly_enabled_for_machine_or_organization,
    lambda do |machine, organization|
      where(organization_id: organization.id).or(
        SecurityContainerConfig.where(machine_id: machine.id)
      ).explicitly_enabled
    end
  )

  class << self
    def for_machines
      where.not(machine_id: nil)
    end

    def for_machine(mid)
      find_by(machine_id: mid)
    end

    def for_organizations
      where.not(organization_id: nil)
    end

    def for_organization(oid)
      find_by(organization_id: oid)
    end

    def explicitly_enabled
      where(enabled_outside_of_requirement: true)
    end

    SecurityContainer::TEST_TYPES.each do |ttype|
      define_method "for_#{ttype}_test_type" do
        joins(:security_container).where(security_containers: { test_types: "{#{ttype}}" })
      end
    end
  end

  def args_hash
    values.each_with_object({}) do |(k, v), h|
      stripped = v.strip
      h[k.to_sym] = stripped.empty? ? EMPTY_CONFIG_VALUE : stripped.gsub(/ /, SecurityContainer::SPACE_PLACEHOLDER)
    end
  end

  private

  # Validation Definitions
  def values_contains_all_arguments
    values_has_all_keys?
  end

  def values_has_all_keys?
    return false unless security_container
    unless security_container.default_config.keys.sort == values.keys.sort
      errors.add :values, 'must contain exact arguments'
      return false
    end
    true
  end

  def resolved_organization
    organization || machine.organization
  end

  def initialize_config_values
    self.values = security_container&.default_config if values.blank?
    self.values = (values || {}).each_with_object({}) do |(k, v), h|
      h[k] = v.to_s if v.respond_to?(:to_s)
    end
  end
end
