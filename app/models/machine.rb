# frozen_string_literal: true

# == Schema Information
#
# Table name: machines
#
#  id                 :integer          not null, primary key
#  organization_id    :integer
#  ip                 :inet
#  fqdn               :string
#  description        :text
#  machine_status     :integer          default("pending"), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  name               :string           not null
#  use_fqdn_as_target :boolean          default(FALSE), not null
#
# Indexes
#
#  index_machines_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_machines_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_bd87ec17a7  (organization_id => organizations.id) ON DELETE => cascade
#

require 'resolv'

class Machine < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Attribute Information
  enum machine_status: %i[pending passing failing warning error]
  RANKED_ASSESSMENT_STATUSES = (Assessment::RANKED_ASSESSMENT_STATUSES.dup << :pending).freeze
  RFC1918_10_RANGE = '10.0.0.0/8'
  RFC1918_172_RANGE = '172.16.0.0/12'
  RFC1918_192_RANGE = '192.168.0.0/16'

  # Validations
  # NOTE: It is not ideal to check for uniqueness at the application level, but since we do not want
  # to treat "blank" as a unique value, we need to manually check for uniqueness
  validates :ip, presence: true, if: proc { |a| a.fqdn.blank? }
  validates :ip, uniqueness: { scope: :organization_id }, allow_blank: true
  validates :ip, exclusion: { in: [IPAddr.new('127.0.0.1')], message: 'cannot be 127.0.0.1' }
  validates :organization, presence: true
  validates :name, presence: true
  validates :name, uniqueness: { scope: :organization_id }
  validates :name, length: { maximum: 36 }
  validates_with SingleIpValidator
  include FqdnValidator
  # Prevent Rails' default ip= from clobbering the attempted IP
  include InetInitializationValidator

  validates(
    :use_fqdn_as_target,
    inclusion: { in: [false], message: 'cannot be true if fqdn is not present' },
    if: proc { |a| a.fqdn.blank? }
  )

  # Associations
  belongs_to :organization, inverse_of: :machines
  has_many :docker_commands, inverse_of: :machine
  has_many :assessments, inverse_of: :machine
  has_many :security_container_configs, inverse_of: :machine
  has_many :scan_schedules, inverse_of: :machine, class_name: 'MachineScanSchedule'
  has_many :services, inverse_of: :machine
  has_many :ssh_services, inverse_of: :machine
  has_many :web_application_services, inverse_of: :machine
  has_many :service_discoveries, inverse_of: :machine
  has_one :ssh_key_pair_assignment, inverse_of: :machine
  has_one :ssh_key_pair, through: :ssh_key_pair_assignment
  has_many :result_ignore_rules, as: :ignore_scope, inverse_of: :ignore_scope, dependent: :destroy
  has_many :ping_connectivity_checks, inverse_of: :machine, validate: false
  has_many :ssh_connectivity_checks, inverse_of: :machine, validate: false

  # Callback Declarations
  before_validation :generate_name, if: 'name.blank?'

  include OrganizationErrorWatcher
  watch_for_organization_errors UnreachableMachineError, UnableToPingMachineError, UnableToSshToMachineError,
                                attributes: %i[ip fqdn]

  # This will destroy any machine-specific organization errors when this machine is destroyed
  destroy_associated_organization_errors OrganizationError

  # Delegations
  delegate :to_s, to: :ip, prefix: true

  class << self
    # Return the set of machines with an enabled configuration for the given security container
    def with_configuration_for(container)
      joins(:security_container_configs)
        .where(
          security_container_configs: { security_container_id: container.id, enabled_outside_of_requirement: true }
        )
    end

    def rfc1918
      where('ip << ? OR ip << ? OR ip << ?', RFC1918_10_RANGE, RFC1918_172_RANGE, RFC1918_192_RANGE)
    end
  end

  def all_docker_commands
    DockerCommand.left_joins(:assessments).where(
      'assessments.machine_id = ? OR docker_commands.machine_id = ?', id, id
    ).group(:id)
  end

  def target_address
    return fqdn if use_fqdn_as_target
    ip ? ip_to_s : fqdn
  end

  def latest_assessment_stats
    assessments.latest_results_stats
  end

  def assessment_status
    Assessment.status_from_stats latest_assessment_stats
  end

  def ssh_key_values
    # FIXME: handle the SshConfig DNE case more gracefully
    use_relay_ssh_key = organization.configuration.use_relay_ssh_key
    {
      ssh_user: use_relay_ssh_key ? '%{ssh_user}' : ssh_key_pair.try(:username),
      ssh_key: use_relay_ssh_key ? '%{ssh_key}' : ssh_key_pair.try(:key)
    }
  end

  # Docker Commands created for an Organization also apply to the associated machines. This method
  # will return the more recent of any Docker Commands created for both the parent Organization and
  # the current instance of Machine
  def latest_command
    DockerCommand.where(organization: organization_id).or(DockerCommand.where(machine: id)).order(:created_at).last
  end

  def latest_results
    MachineWithLatestResults.new(self).latest_results
  end

  def rbac_parent
    organization
  end

  def self.find(identifier)
    if ip_address? identifier.to_s
      find_by!(ip: identifier)
    else
      super identifier
    end
  end

  private

  private_class_method def self.ip_address?(value)
    Resolv::IPv4::Regex =~ value
  end

  def generate_name
    self.name = name.blank? ? SecureRandom.uuid : name
  end
end
