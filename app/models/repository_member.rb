# frozen_string_literal: true

# == Schema Information
#
# Table name: repository_members
#
#  id                          :integer          not null, primary key
#  user_id                     :integer          not null
#  security_test_repository_id :integer          not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_repository_members_on_security_test_repository_id  (security_test_repository_id)
#  index_repository_members_on_u_id_and_s_t_r_id            (user_id,security_test_repository_id) UNIQUE
#  index_repository_members_on_user_id                      (user_id)
#
# Foreign Keys
#
#  fk_rails_2a2cfda3a3  (user_id => users.id) ON DELETE => cascade
#  fk_rails_e2d371a88f  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#

class RepositoryMember < ApplicationRecord
  include Authority::Abilities

  attr_accessor :skip_roles, :role_type

  belongs_to :user
  belongs_to :security_test_repository

  validates :user, uniqueness: { scope: :security_test_repository_id }

  before_save :create_role, unless: :skip_roles
  before_destroy :destroy_roles, unless: :skip_roles

  private

  def destroy_roles
    determine_roles.each { |role| user.remove_role role, security_test_repository }
  end

  def create_role
    if %w[admin reader].include? role_type.to_s
      # add_role behaves like first_or_create
      role = user.add_role "security_test_repository_#{role_type}", security_test_repository
      return role if role
    end
    errors.add(:base, "Unable to add #{role_type} role for user")
    throw :abort
  end

  def determine_roles
    user.roles.each_with_object([]) do |role, a|
      a << role.name.to_sym if security_test_repository.id == role.resource_id && \
                               role.resource_type == 'SecurityTestRepository'
    end
  end
end
