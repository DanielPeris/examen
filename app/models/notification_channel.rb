# frozen_string_literal: true

# == Schema Information
#
# Table name: notification_channels
#
#  id              :integer          not null, primary key
#  enabled         :boolean          default(TRUE)
#  event           :string
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_notification_channels_on_event                      (event)
#  index_notification_channels_on_organization_id_and_event  (organization_id,event) UNIQUE
#

class NotificationChannel < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Validations
  validates :event,
            presence: true,
            uniqueness: {
              scope: :organization_id,
              case_sensitive: false
            }
  validates :organization, presence: true

  # Associations
  belongs_to :organization, inverse_of: :notification_channels
end
