# frozen_string_literal: true

# This is a specific instance of a template following the template design
# pattern. See the base class ResultAbstractErrorAttributes for details.

class ResultMalformedParamsErrorAttributes < ResultAbstractErrorAttributes
  private

  def generate_title
    'Missing Critical Information'
  end

  def generate_description(exception)
    "Security test results are missing critical information: #{exception.message}"
  end

  def generate_nid
    'parameter-missing:10010'
  end
end
