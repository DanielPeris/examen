# frozen_string_literal: true

class DockerSwarm
  Docker.url = ENV['SWARM_MASTER_URL']
  DOCKER_NETWORK_NAME = ENV.fetch('DOCKER_NETWORK_NAME')
  DOCKER_DNS_SERVERS = ENV.fetch('DOCKER_DNS_SERVERS')&.split(',')

  class << self
    include ContainerPathHelpers

    # rubocop:disable Metrics/ParameterLists
    def queue_container(name, image, args, targets, secret, relay_opts = {})
      image = image.to_s
      container_options = ContainerEnvironmentBuilder.build_options_hash(name, image, args, targets, secret)
      send_or_start_container(container_options, relay_opts)
    end
    # rubocop:enable Metrics/ParameterLists

    def queue_discovery_container(discovery, relay_opts = {})
      discovery_environment = ContainerEnvironmentBuilder.build_discovery_environment(discovery)
      container_opts = {
        'Image' => "#{NORAD_REGISTRY}/service-discovery:latest",
        'Env' => discovery_environment,
        'Cmd' => [discovery.machine.target_address]
      }
      send_or_start_container(container_opts, relay_opts)
    end

    def queue_machine_connectivity_container(check, relay_opts = {})
      connectivity_environment = ContainerEnvironmentBuilder.build_connectivity_check_environment(check)
      container_opts = {
        'Image' => "#{NORAD_REGISTRY}/#{check.class::CONTAINER_NAME}:#{check.class::CONTAINER_VERSION}",
        'Env' => connectivity_environment,
        'Cmd' => check.container_args
      }
      send_or_start_container(container_opts, relay_opts)
    end

    private

    def send_or_start_container(container_opts, relay_opts = {})
      image = container_opts['Image']
      if relay_opts[:relay_queue]
        send_to_relay(container_opts.merge(creds_for_image(image)), relay_opts)
      else
        start_container(container_opts)
      end
    end

    # Some containers, especially official ones (service-discovery) may not be in the DB,
    # but run them anyway if they belong to official repos
    def repository_for_image(image)
      SecurityContainer.by_full_paths(image).first&.security_test_repository ||
        SecurityTestRepository.find_by(host: repository_hosts(image), official: true)
    end

    def creds_for_image(image)
      repository = repository_for_image(image)
      if repository&.username.present?
        { 'Username' => repository.username, 'Password' => repository.password }
      else
        {}
      end
    end

    def start_container(container_options)
      image = container_options.fetch('Image')
      raise(NoradApiError, 'External container requested for Norad Cloud!') if containers_from_unofficial_repos?(image)
      pull_image(image)
      Rails.logger.debug "Starting container for: #{container_options['Image']}"
      container_options.merge!(network_options)
      container = Docker::Container.create(container_options)
      container.start
    end

    def send_to_relay(container_options, relay_opts)
      container_options['encoded_relay_private_key'] = relay_opts[:relay_secret]
      statement = { docker_options: container_options }
      Publisher.publish relay_opts[:relay_exchange], statement, relay_opts[:relay_queue]
    end

    # Call this method before starting a container in order to ensure that the node has the latest
    # version of the image you are wanting to run
    def pull_image(image)
      opts = { fromImage: image }
      creds = creds_for_image(image)

      # Analogous to `docker pull <image_name>`
      if creds.present?
        Docker::Image.create opts, creds
      else
        Docker::Image.create opts
      end
    end

    def network_options
      { NetworkMode: DOCKER_NETWORK_NAME, Dns: DOCKER_DNS_SERVERS }
    end
  end
end
