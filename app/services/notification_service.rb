# frozen_string_literal: true

# A generic notification service.
class NotificationService
  class << self
    def scan_complete(docker_command)
      return if docker_command.finished_at.present? # a scan should only finish once
      docker_command.update_columns(finished_at: DateTime.current)
      org = docker_command.resolved_organization
      NotificationMailer.scan_complete(docker_command.id).deliver_later if org.notification_enabled?('scan_complete')
    end
  end
end
