# frozen_string_literal: true

module ContainerQueryHelpers
  extend ActiveSupport::Concern

  included do
    private

    def enabled_containers
      (explicitly_enabled_container_ids + required_container_ids).uniq.map { |id| { security_container_id: id } }
    end

    def required_container_ids
      organization.required_containers.pluck(:id)
    end

    def explicitly_enabled_container_ids
      SecurityContainerConfig
        .includes(:security_container)
        .explicitly_enabled
        .where('(machine_id IN (?) OR organization_id = ?)', machine_ids, organization.id)
        .pluck(:security_container_id)
    end
  end
end
