# frozen_string_literal: true

module V1
  class WebApplicationConfigsController < ApplicationController
    before_action :set_service, only: :create
    before_action :set_config, only: %i[update show destroy]

    def update
      authorize_action_for @config
      if @config.update(config_params)
        render json: @config
      else
        render_errors_for(@config)
      end
    end

    def show
      authorize_action_for @config
      render json: @config
    end

    def create
      authorize_action_for WebApplicationConfig, in: @web_app.organization
      config = @web_app.build_web_application_config(config_params)
      if config.save
        render json: config
      else
        render_errors_for(config)
      end
    end

    def destroy
      authorize_action_for @config
      @config.destroy
      head :no_content
    end

    private

    def set_service
      @web_app = WebApplicationService.find(params[:service_id])
    end

    def config_params
      params.require(:web_application_config).permit(
        :auth_type,
        :starting_page_path,
        :login_form_username_field_name,
        :login_form_password_field_name,
        :url_blacklist
      )
    end

    def set_config
      @config = WebApplicationConfig.find(params[:id])
    end
  end
end
