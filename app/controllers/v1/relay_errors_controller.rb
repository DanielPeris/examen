# frozen_string_literal: true

module V1
  class RelayErrorsController < ApplicationController
    before_action :set_relay
    before_action :require_signature
    skip_before_action :require_api_token, only: %i[create destroy]

    def create
      error = @relay.organization.organization_errors.build(creation_params)
      if error.save
        head :no_content
      else
        render_errors_for(error)
      end
    rescue ActiveRecord::RecordNotUnique
      # The Relay blindly creates errors when things fail
      head :no_content
    end

    def destroy
      error = RelayError.find_by!(organization: @relay.organization, type: 'RelayQueueError')
      error.destroy!
      head :no_content
    rescue ActiveRecord::RecordNotFound
      # The Relay blindly removes errors when things succeed
      head :no_content
    end

    private

    def require_signature
      require_pubpriv_signature @relay.public_key
    end

    def creation_params
      # For now, this is the only type of RelayError creatable from the Relay
      params.require(:relay_error).permit(:type).merge(type: 'RelayQueueError', errable: @relay)
    end

    def set_relay
      @relay ||= DockerRelay.find(params[:docker_relay_id])
    end
  end
end
