# frozen_string_literal: true

# == Schema Information
#
# Table name: repository_whitelist_entries
#
#  id                          :integer          not null, primary key
#  organization_id             :integer          not null
#  security_test_repository_id :integer          not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_o_s_t_repositories_on_o_id_and_s_t_r_id  (organization_id,security_test_repository_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_27555b3c57  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#  fk_rails_c3b0a8cf92  (organization_id => organizations.id) ON DELETE => cascade
#

module V1
  class RepositoryWhitelistEntriesController < ApplicationController
    before_action :load_repository, only: %i[create index]
    before_action :set_entry, only: :destroy

    # GET /security_test_repositories/:security_test_repository_id/repository_whitelist_entries
    def index
      authorize_action_for RepositoryWhitelistEntry, in: @repository
      @entries = RepositoryWhitelistEntry.where(security_test_repository_id: @repository.id)
      render json: @entries
    end

    # POST /security_test_repositories/:security_test_repository_id/repository_whitelist_entries
    def create
      authorize_action_for RepositoryWhitelistEntry, in: @repository, org: organization
      @entry = @repository.repository_whitelist_entries.build(entry_params)

      if @entry.save
        render json: @entry
      else
        render_errors_for @entry
      end
    end

    # DELETE /repository_whitelist_entries/:id
    def destroy
      if @entry.destroy
        head :no_content
      else
        render_errors_for @entry
      end
    end

    private

    def set_entry
      @entry = RepositoryWhitelistEntry.find(params[:id])
      authorize_action_for @entry
    end

    def entry_params
      params.require(:repository_whitelist_entry).permit(:organization_id)
    end

    def organization
      @organization ||= Organization.find(entry_params[:organization_id])
    end

    def load_repository
      @repository = SecurityTestRepository.find(params[:security_test_repository_id])
    end
  end
end
