# frozen_string_literal: true

module V1
  class MachineScanSummariesController < ApplicationController
    before_action :load_machine

    # Show scan summaries for a single machine
    #
    # GET /machines/{machine.id}/scan_summary
    def show
      summary = MachineScanSummary.new(machine: @machine)
      authorize_action_for summary, in: @machine.rbac_parent
      render json: summary, only: { assessments_summary: %i[created_at state] }
    end

    private

    def load_machine
      @machine = Machine.find(params[:machine_id])
    end
  end
end
