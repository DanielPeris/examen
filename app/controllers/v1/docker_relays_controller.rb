# frozen_string_literal: true

# == Schema Information
#
# Table name: docker_relays
#
#  id              :integer          not null, primary key
#  organization_id :integer          not null
#  public_key      :text             not null
#  queue_name      :string           not null
#  state           :integer          default(0), not null
#  last_heartbeat  :datetime         not null
#  verified        :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_docker_relays_on_organization_id  (organization_id)
#  index_docker_relays_on_public_key       (public_key) UNIQUE
#  index_docker_relays_on_queue_name       (queue_name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_696422edd6  (organization_id => organizations.id)
#

module V1
  class DockerRelaysController < ApplicationController
    before_action :set_relay, only: %i[update heartbeat]
    skip_before_action :require_api_token, only: %i[destroy show create heartbeat]
    before_action :require_api_token_or_signature, only: %i[destroy show heartbeat]

    def index
      org = Organization.find(params[:organization_id])
      authorize_action_for DockerRelay, in: org
      render json: org.docker_relays.order(created_at: :desc)
    end

    def create
      org = fetch_organization_by_token
      return missing_or_invalid_org_token_response if org.nil?

      relay = org.docker_relays.build(relay_params)
      if relay.save
        render json: relay, include_public_keys: true
      else
        render json: { errors: relay }, status: :unprocessable_entity
      end
    end

    def update
      authorize_action_for @relay
      if @relay.update(update_params)
        render json: @relay
      else
        render_errors_for(@relay)
      end
    end

    def heartbeat
      @relay.last_reported_version = params.dig(:version, :current)
      @relay.process_heartbeat
      render json: @relay
    end

    def destroy
      @relay.destroy!
      head :no_content
    end

    def show
      render json: @relay, include_public_keys: true
    end

    private

    def fetch_organization_by_token
      Organization.by_token(org_token)
    rescue ActiveRecord::RecordNotFound, ActionController::ParameterMissing
      nil
    end

    def missing_or_invalid_org_token_response
      render(json: { errors: 'Organization Token is missing or invalid' }, status: :unprocessable_entity)
    end

    def set_relay
      @relay = DockerRelay.find(params[:id])
    end

    def org_token
      params.require(:organization_token)
    end

    def relay_params
      params.require(:docker_relay).permit(:public_key)
    end

    def update_params
      params.require(:docker_relay).permit(:verified)
    end

    def require_api_token_or_signature
      @relay ||= DockerRelay.find(params[:id])
      if signed_request?
        require_pubpriv_signature @relay.public_key
      else
        require_api_token
        authorize_action_for @relay
      end
    end
  end
end
