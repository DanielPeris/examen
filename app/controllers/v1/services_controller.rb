# frozen_string_literal: true

module V1
  class ServicesController < ApplicationController
    before_action :set_service, only: %i[show update destroy]
    before_action :set_machine, only: %i[create index]
    skip_before_action :require_api_token, only: :create
    before_action :require_api_token_or_signature, only: :create

    def index
      authorize_action_for Service, in: @machine.organization
      render json: @machine.services
    end

    def show
      render json: @service
    end

    def update
      if @service.update(service_update_params)
        render json: @service
      else
        render_errors_for(@service)
      end
    end

    def create
      authorize_action_for Service, in: @machine.organization unless via_discovery?
      service = initialize_new_service
      if service.save
        render json: service
      else
        render_errors_for(service)
      end
    end

    def destroy
      @service.destroy
      head :no_content
    end

    private

    def set_service
      @service = Service.find(params[:id])
      authorize_action_for @service
    end

    def service_creation_params
      scp = params.require(:service).permit(
        :name, :description, :port, :port_type, :encryption_type, :type, :allow_brute_force, :discovered
      )
      if via_discovery?
        scp[:application_type] = discovered_application_type(scp)
      end
      scp[:application_type] ||= application_type
      scp
    end

    def service_update_params
      scp = params.require(:service).permit(
        :name, :description, :port, :port_type, :encryption_type, :allow_brute_force, :discovered
      )
      scp[:application_type] ||= application_type
      scp
    end

    def application_type_params
      params.require(:service).permit(application_type: %i[name port protocol])
    end

    def discovered_application_type(scp)
      return unless scp[:port].present? && scp[:port_type].present?
      ApplicationType.where(port: scp[:port], transport_protocol: scp[:port_type]).first
    end

    def application_type
      cst = application_type_params[:application_type]
      return unless cst
      return unless cst[:port].present? && cst[:protocol].present?
      ApplicationType.where(name: cst[:name], port: cst[:port], transport_protocol: cst[:protocol]).first
    end

    def set_machine
      @machine = Machine.find(params[:machine_id])
    end

    def require_api_token_or_signature
      return require_api_token unless via_discovery?
      require_container_secret_signature(@machine.service_discoveries.current&.shared_secret || SecureRandom.hex)
    end

    def initialize_new_service
      return @machine.services.new(service_creation_params) unless via_discovery?
      @machine.services.build_or_update_discovered_service(service_creation_params)
    end

    def via_discovery?
      params[:via_discovery].to_s == 'true'
    end
  end
end
