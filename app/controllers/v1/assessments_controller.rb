# frozen_string_literal: true

module V1
  class AssessmentsController < ApplicationController
    before_action :set_assessment, only: %i[show update]
    before_action :require_container_secret_signature, only: :update
    before_action :set_machine, only: %i[index latest]
    skip_before_action :require_api_token, only: :update

    def index
      authorize_action_for Assessment, in: @machine.organization
      render json: assessments
    end

    def update
      return head :no_content if transition_state
      head :not_modified
    end

    def latest
      params[:limit] = 'latest'
      index
    end

    def show
      authorize_action_for @assessment
      render json: @assessment
    end

    private

    def transition_state
      @assessment.complete! if update_params[:state] == 'complete'
    end

    def update_params
      params.require(:assessment).permit(:state)
    end

    def set_assessment
      @assessment = Assessment.find(params[:id])
    end

    def set_machine
      @machine = Machine.find(params[:machine_id])
    end

    def assessments
      return @machine.assessments.for_docker_command(params[:docker_command_id]) if params[:docker_command_id]
      scoped_assessments
    end

    def scoped_assessments
      limit = params[:limit].to_s
      return @machine.assessments.latest if limit == 'latest'
      return @machine.assessments.order(created_at: :desc) if limit == 'all'
      return @machine.assessments.order(docker_command_id: :desc) if limit == 'all'
      @machine.assessments.most_recent_by_docker_command_id
    end

    def container_secret
      @assessment.security_container_secret&.secret
    end
  end
end
