# frozen_string_literal: true

module V1
  class ResultExportersController < ApplicationController
    def create
      authorize_action_for ResultExporter, in: organization
      organization.result_export_queues.each do |queue|
        queue.export(params[:result_ids])
      end

      render json: { message: "Queuing #{params[:result_ids].length} results for export" }, status: :created
    end

    private

    def organization
      @organization ||= lookup_organization
    end

    def lookup_organization
      organization_ids = Organization.joins(machines: { assessments: :results })
                                     .where(results: { id: params[:result_ids] }).distinct.pluck(:id)
      # If the request is coming from the UI, the number of organization IDs will always be 1. API
      # users could attempt to export for multiple organizations. To support that, we would need to
      # verify the RBAC for each. For now, let's just not allow any funny business.
      raise ActionController::BadRequest, 'Result IDs span multiple organizations' if organization_ids.length != 1
      Organization.find(organization_ids.first)
    end
  end
end
