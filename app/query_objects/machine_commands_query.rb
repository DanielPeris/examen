# frozen_string_literal: true

class MachineCommandsQuery
  attr_reader :relation

  def initialize(machines)
    @machine_relation = machines
    @relation = DockerCommand.where(machine: machine_relation)
  end

  def unique_commands_newer_than(cutoff)
    cutoff ? unique_commands.where('created_at >= ?', cutoff) : unique_commands
  end

  # In this method we take advantage of the PostgreSQL feature known as DISTINCT ON. Most databases
  # only allow you to run DISTINCT on the entire selection of columns, but with Postgres we can take
  # advantage of this functionality to make this query very simple to write.
  # https://stackoverflow.com/questions/3800551/select-first-row-in-each-group-by-group
  #
  # This method intentionally does not take into account that some Docker Commands are created for
  # Organizations. Technically speaking there could be a newer Docker Command that is implicitly
  # created "for" a Machine.
  def unique_commands
    relation
      .select('DISTINCT ON (machine_id) *')
      .order(machine_id: :desc, created_at: :desc, id: :desc)
      .where(machine: machine_relation)
  end

  private

  attr_reader :machine_relation
end
