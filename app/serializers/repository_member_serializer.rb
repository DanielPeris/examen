# frozen_string_literal: true

# == Schema Information
#
# Table name: repository_members
#
#  id                          :integer          not null, primary key
#  user_id                     :integer          not null
#  security_test_repository_id :integer          not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_repository_members_on_security_test_repository_id  (security_test_repository_id)
#  index_repository_members_on_u_id_and_s_t_r_id            (user_id,security_test_repository_id) UNIQUE
#  index_repository_members_on_user_id                      (user_id)
#
# Foreign Keys
#
#  fk_rails_2a2cfda3a3  (user_id => users.id) ON DELETE => cascade
#  fk_rails_e2d371a88f  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#

class RepositoryMemberSerializer < ActiveModel::Serializer
  attributes :id, :security_test_repository_id, :role_type, :created_at, :updated_at
  belongs_to :user

  def role_type
    object.user.roles.find_by!(resource: object.security_test_repository).name.gsub(/security_test_repository_/, '')
  end
end
