# frozen_string_literal: true

# == Schema Information
#
# Table name: custom_jira_configurations
#
#  id                     :integer          not null, primary key
#  title                  :string
#  site_url               :string           not null
#  project_key            :string           not null
#  username_encrypted     :string
#  password_encrypted     :string
#  result_export_queue_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_custom_jira_configurations_on_result_export_queue_id  (result_export_queue_id)
#
# Foreign Keys
#
#  fk_rails_b1be8cc432  (result_export_queue_id => result_export_queues.id) ON DELETE => cascade
#

class CustomJiraConfigurationSerializer < ActiveModel::Serializer
  attributes :id, :title, :project_key, :username, :result_export_queue_id, :site_url, :created_at, :updated_at
end
