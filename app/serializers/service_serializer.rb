# frozen_string_literal: true

# == Schema Information
#
# Table name: services
#
#  id                  :integer          not null, primary key
#  name                :string           not null
#  description         :text
#  port                :integer          not null
#  port_type           :integer          default("tcp"), not null
#  encryption_type     :integer          default("cleartext"), not null
#  machine_id          :integer
#  type                :string           not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  allow_brute_force   :boolean          default(FALSE), not null
#  application_type_id :integer
#  discovered          :boolean          default(FALSE), not null
#
# Indexes
#
#  index_services_on_application_type_id  (application_type_id)
#  index_services_on_machine_id           (machine_id)
#  index_services_on_machine_id_and_port  (machine_id,port) UNIQUE
#  index_services_on_type                 (type)
#
# Foreign Keys
#
#  fk_rails_6a8ba918c1  (application_type_id => application_types.id)
#  fk_rails_b32a34656d  (machine_id => machines.id) ON DELETE => cascade
#

class ServiceSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :description,
             :port,
             :port_type,
             :encryption_type,
             :machine_id,
             :type,
             :allow_brute_force,
             :discovered,
             :created_at,
             :updated_at,
             :application_type

  has_one :service_identity

  def application_type
    return unless object.application_type
    ApplicationTypeSerializer.new(object.application_type).as_json
  end
end
