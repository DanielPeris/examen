# frozen_string_literal: true

# == Schema Information
#
# Table name: organizations
#
#  id         :integer          not null, primary key
#  uid        :string           not null
#  slug       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_organizations_on_slug  (slug) UNIQUE
#  index_organizations_on_uid   (uid) UNIQUE
#

class OrganizationSerializer < ActiveModel::Serializer
  attributes(
    :id,
    :uid,
    :slug,
    :token,
    :machine_count,
    :machine_assessment_summary,
    :contains_enabled_tests
  )

  has_one :primary_relay
  has_one :configuration
  has_many :organization_errors

  def token
    object.token if user_is_org_admin?
  end

  def contains_enabled_tests
    object.contains_enabled_tests?
  end

  def machine_count
    object.machines.count
  end

  def machine_assessment_summary
    return {} unless instance_options[:include_assessment_summary]
    object.machine_assessment_summary
  end

  private

  def user_is_org_admin?
    current_user && current_user.has_role?(:organization_admin, object)
  end

  def current_user
    Thread.current['current_user']
  end
end
