# frozen_string_literal: true

# == Schema Information
#
# Table name: service_identities
#
#  id                 :integer          not null, primary key
#  username_encrypted :string
#  password_encrypted :string
#  service_id         :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_service_identities_on_service_id  (service_id)
#
# Foreign Keys
#
#  fk_rails_b8cfe24029  (service_id => services.id) ON DELETE => cascade
#

class ServiceIdentitySerializer < ActiveModel::Serializer
  attributes :id, :username, :password, :service_id, :created_at, :updated_at
end
