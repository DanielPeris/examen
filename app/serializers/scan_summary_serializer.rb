# frozen_string_literal: true

class ScanSummarySerializer < ActiveModel::Serializer
  attributes :assessments_summary

  def assessments_summary
    object.query.assessments_summary
  end
end
