# frozen_string_literal: true

# == Schema Information
#
# Table name: ssh_key_pair_assignments
#
#  id              :integer          not null, primary key
#  machine_id      :integer          not null
#  ssh_key_pair_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_ssh_key_pair_assignments_on_machine_id       (machine_id) UNIQUE
#  index_ssh_key_pair_assignments_on_ssh_key_pair_id  (ssh_key_pair_id)
#
# Foreign Keys
#
#  fk_rails_05e7648bab  (ssh_key_pair_id => ssh_key_pairs.id) ON DELETE => cascade
#  fk_rails_7c3731b704  (machine_id => machines.id) ON DELETE => cascade
#

class SshKeyPairAssignmentSerializer < ActiveModel::Serializer
  attributes :id, :machine_id, :ssh_key_pair_id, :created_at, :updated_at
end
