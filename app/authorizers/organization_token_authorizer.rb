# frozen_string_literal: true

class OrganizationTokenAuthorizer < ApplicationAuthorizer
  def readable_by?(user)
    admin?(user, org)
  end

  def updatable_by?(user)
    admin?(user, org)
  end

  private

  def org
    resource.organization
  end
end
