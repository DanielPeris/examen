# frozen_string_literal: true

class IaasDiscoveryAuthorizer < ApplicationAuthorizer
  def self.creatable_by?(user, options)
    admin?(user, options[:in])
  end

  def self.readable_by?(user, options)
    admin?(user, options[:in])
  end

  def readable_by?(user)
    admin?(user, resource.organization)
  end
end
