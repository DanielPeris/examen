# frozen_string_literal: true

class SecurityContainerAuthorizer < ApplicationAuthorizer
  class << self
    # Any user can list containers. Finer-level attribute controls are handled in the serializer
    def readable_by?(_user)
      true
    end

    # Only repository admins can create a security container
    def creatable_by?(user, options)
      admin?(user, options[:in])
    end

    def admin?(user, repository = nil)
      repository && user.has_role?(:security_test_repository_admin, repository)
    end

    def reader?(user, repository = nil)
      repository && user.has_role?(:security_test_repository_reader, repository)
    end

    # Used to redact sensitive attributes from this collection
    def serializer(user, options = {})
      repository = options[:in]
      if repository.blank? || repository.public || admin?(user, repository) || reader?(user, repository)
        SecurityContainerSerializer
      else
        RedactedSecurityContainerSerializer
      end
    end
  end

  # Any user can retrieve a container. Finer-level attribute controls are handled in the serializer
  def readable_by?(_user)
    true
  end

  # Only this repository admin can update this container
  def updatable_by?(user)
    admin?(user)
  end

  # Only this repository admin can delete this container
  def deletable_by?(user)
    admin?(user)
  end

  def admin?(user)
    user.has_role? :security_test_repository_admin, repository
  end

  # Used to redact sensitive attributes from this resource
  def serializer(user)
    if redact?(user)
      RedactedSecurityContainerSerializer
    else
      SecurityContainerSerializer
    end
  end

  private

  def reader?(user)
    user.has_role? :security_test_repository_reader, repository
  end

  def redact?(user)
    !(admin?(user) || reader?(user) || repository.public || included_in_whitelist_for?(user))
  end

  def repository
    resource.security_test_repository
  end

  def included_in_whitelist_for?(user)
    user.whitelisted_containers.select(:id).exists? resource.id
  end
end
