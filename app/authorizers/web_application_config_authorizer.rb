# frozen_string_literal: true

class WebApplicationConfigAuthorizer < ApplicationAuthorizer
  def self.creatable_by?(user, options)
    admin?(user, options[:in])
  end

  def updatable_by?(user)
    admin?(user, org)
  end

  # There is no index action
  def self.readable_by?(_user, _options)
    false
  end

  def readable_by?(user)
    admin?(user, org) || reader?(user, org)
  end

  def deletable_by?(user)
    admin?(user, org)
  end

  private

  def org
    resource.web_application_service.organization
  end
end
