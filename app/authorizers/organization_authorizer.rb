# frozen_string_literal: true

class OrganizationAuthorizer < ApplicationAuthorizer
  def self.creatable_by?(_user)
    true
  end

  def updatable_by?(user)
    admin?(user, resource)
  end

  def readable_by?(user)
    reader?(user, resource) || admin?(user, resource)
  end

  def deletable_by?(user)
    admin?(user, resource)
  end
end
