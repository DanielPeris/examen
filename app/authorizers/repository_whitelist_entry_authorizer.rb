# frozen_string_literal: true

# == Schema Information
#
# Table name: repository_whitelist_entries
#
#  id                          :integer          not null, primary key
#  organization_id             :integer          not null
#  security_test_repository_id :integer          not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_r_w_entries_on_o_id_and_s_t_r_id  (organization_id,security_test_repository_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_27555b3c57  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#  fk_rails_c3b0a8cf92  (organization_id => organizations.id) ON DELETE => cascade
#

class RepositoryWhitelistEntryAuthorizer < ApplicationAuthorizer
  class << self
    def creatable_by?(user, options)
      repository = options.fetch(:in)
      (admin?(user, repository) || reader?(user, repository) || repository.public) &&
        options[:org].admins.exists?(user.id)
    end

    def readable_by?(user, options)
      repository = options.fetch(:in)
      admin?(user, repository) || reader?(user, repository) || repository.public
    end

    private

    def admin?(user, repository)
      user.has_role? :security_test_repository_admin, repository
    end

    def reader?(user, repository)
      user.has_role? :security_test_repository_reader, repository
    end
  end

  def deletable_by?(user)
    (admin?(user) || reader?(user) || resource.security_test_repository.public) &&
      resource.organization.admins.exists?(user.id)
  end

  private

  def admin?(user)
    user.has_role? :security_test_repository_admin, resource.security_test_repository
  end

  def reader?(user)
    user.has_role? :security_test_repository_reader, resource.security_test_repository
  end
end
