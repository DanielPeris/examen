# frozen_string_literal: true

class DestroyExpiredRecordsJob
  include ActsAsRecurringJob

  # Each class listed here must implement ::expired which returns an ActiveRecord::Relation
  # or Array of instances of the class.
  PERISHABLE_CLASSES = [MachineConnectivityCheck, SecurityContainerSecret].freeze

  class << self
    def job_action
      Rails.logger.info "Removing expired records for classes #{PERISHABLE_CLASSES}"
      PERISHABLE_CLASSES.each { |klass| destroy_expired_records(klass) }
      Rails.logger.info "Finished removing expired records for classes #{PERISHABLE_CLASSES}"
    end

    private

    def destroy_expired_records(klass)
      Rails.logger.info "Checking for expired #{klass.to_s.pluralize} over 24 hours old..."
      expired_records = klass.expired
      if expired_records.present?
        count = expired_records.count
        expired_records.destroy_all
        Rails.logger.info "#{count} records were destroyed!"
      else
        Rails.logger.info "No expired #{klass.to_s.pluralize} were found."
      end
    end
  end
end
