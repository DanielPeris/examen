# frozen_string_literal: true

require './lib/norad_exception_notifier'

module ActsAsRecurringJob
  extend ActiveSupport::Concern

  included do
    @queue = :recurring
    include NoradExceptionNotifier
  end

  class_methods do
    def perform(*args)
      job_action(*args)
    rescue StandardError => exception
      # Airbrake doesn't wrap errors for plain Ruby classes, so catch the
      # notification here, notify Airbrake manually and re-raise the exception to Resque.
      # For more investigation, see where the Airbrake wrappers are loaded here:
      # https://github.com/airbrake/airbrake/tree/master/lib/airbrake/rails
      notify_airbrake(exception)
      raise exception
    end
  end
end
