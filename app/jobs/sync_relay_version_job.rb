# frozen_string_literal: true

# This job simply serves to keep the Relay version cache hot.
class SyncRelayVersionJob < ApplicationJob
  include ActsAsRecurringJob

  class << self
    def job_action
      RelayImageInfoFetcher.fetch_and_cache_latest_version
    end
  end
end
