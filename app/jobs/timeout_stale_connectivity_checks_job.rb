# frozen_string_literal: true

class TimeoutStaleConnectivityChecksJob
  include ActsAsRecurringJob

  class << self
    def job_action
      Rails.logger.info 'Checking for stale Connectivity Checks...'
      stale_checks = MachineConnectivityCheck.stale
      stale_checks.find_each do |check|
        # Assume the check has failed this point. Since there's no easy way to actually
        # stop the potentially running container, the check could still succeed at which point
        # this error will be removed.
        check.update!(status: 'failing')
      end
      Rails.logger.info "Stale Connectivity Checks check complete! #{stale_checks.count} were timed out."
    end
  end
end
