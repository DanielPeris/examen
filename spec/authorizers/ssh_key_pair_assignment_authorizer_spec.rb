# frozen_string_literal: true

require 'rails_helper'

describe SshKeyPairAssignmentAuthorizer, type: :authorizer do
  before :each do
    @org = create :organization
    @admin_user = create :user
    @admin_user.add_role :organization_admin, @org
    @reader_user = create :user
    @reader_user.add_role :organization_reader, @org
    @unprivileged_user = create :user

    machine = create :machine, organization: @org
    @assignment = create :ssh_key_pair_assignment, machine: machine
  end

  context 'ssh key pair assignment action' do
    it 'try to create' do
      expect(SshKeyPairAssignment.authorizer).to be_creatable_by(@admin_user, in: @org)
      expect(SshKeyPairAssignment.authorizer).to_not be_creatable_by(@reader_user, in: @org)
      expect(SshKeyPairAssignment.authorizer).to_not be_creatable_by(@unprivileged_user, in: @org)
    end

    it 'try to delete' do
      expect(@assignment.authorizer).to be_deletable_by(@admin_user)
      expect(@assignment.authorizer).to_not be_deletable_by(@reader_user)
      expect(@assignment.authorizer).to_not be_deletable_by(@unprivileged_user)
    end
  end
end
