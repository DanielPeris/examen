# frozen_string_literal: true

require 'rails_helper'

describe RepositoryWhitelistEntryAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @reader_user = create :user
    @org = @admin_user.organizations.first
    @repository = create :private_repository
    @admin_user.add_role :security_test_repository_admin, @repository
    @reader_user.add_role :security_test_repository_reader, @repository
    @entry = create :repository_whitelist_entry,
                    organization: @org,
                    security_test_repository: @repository
    @unprivileged_user = create :user
  end

  context 'repository_whitelist_entry action' do
    it 'try to create' do
      expect(RepositoryWhitelistEntry.authorizer).to be_creatable_by(@admin_user, in: @repository, org: @org)
      @admin_user.remove_role :organization_admin, @org
      @admin_user.add_role :organization_reader, @org
      expect(RepositoryWhitelistEntry.authorizer).not_to be_creatable_by(@admin_user, in: @repository, org: @org)
      expect(RepositoryWhitelistEntry.authorizer).not_to be_creatable_by(@unprivileged_user, in: @repository, org: @org)
      expect(RepositoryWhitelistEntry.authorizer).not_to be_creatable_by(@reader_user, in: @repository, org: @org)
    end

    it 'try to delete' do
      expect(@entry.authorizer).to be_deletable_by(@admin_user)
      expect(@entry.authorizer).not_to be_deletable_by(@unprivileged_user)
      expect(@entry.authorizer).not_to be_deletable_by(@reader_user)
    end

    it 'try to read from class level' do
      expect(RepositoryWhitelistEntry.authorizer).to be_readable_by(@admin_user, in: @repository)
      expect(RepositoryWhitelistEntry.authorizer).to be_readable_by(@reader_user, in: @repository)
      expect(RepositoryWhitelistEntry.authorizer).not_to be_readable_by(@unprivileged_user, in: @repository)
    end
  end
end
