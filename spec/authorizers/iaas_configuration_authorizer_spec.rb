# frozen_string_literal: true

require 'rails_helper'

describe IaasConfigurationAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @reader_user = create :user
    @reader_user_other = create :user
    @admin_user_other = create :user
    @org1 = create :organization
    @org2 = create :organization
    @admin_user.add_role :organization_admin, @org1
    @admin_user_other.add_role :organization_admin, @org2
    @reader_user.add_role :organization_reader, @org1
    @reader_user_other.add_role :organization_reader, @org2
    @iaas_configuration = create :iaas_configuration, organization: @org1
    @options = { in: @org1 }
  end

  it 'allows org admins to create' do
    expect(IaasConfiguration.authorizer).to be_creatable_by(@admin_user, @options)
    expect(IaasConfiguration.authorizer).to_not be_creatable_by(@admin_user_other, @options)
    expect(IaasConfiguration.authorizer).to_not be_creatable_by(@reader_user, @options)
    expect(IaasConfiguration.authorizer).to_not be_creatable_by(@reader_user_other, @options)
  end

  it 'allows org admins to delete' do
    expect(@iaas_configuration.authorizer).to be_deletable_by(@admin_user)
    expect(@iaas_configuration.authorizer).to_not be_deletable_by(@admin_user_other)
    expect(@iaas_configuration.authorizer).to_not be_deletable_by(@reader_user)
    expect(@iaas_configuration.authorizer).to_not be_deletable_by(@reader_user_other)
  end

  it 'allows org admins to read' do
    expect(@iaas_configuration.authorizer).to be_readable_by(@admin_user)
    expect(@iaas_configuration.authorizer).to_not be_readable_by(@admin_user_other)
    expect(@iaas_configuration.authorizer).to_not be_readable_by(@reader_user)
    expect(@iaas_configuration.authorizer).to_not be_readable_by(@reader_user_other)
  end

  it 'allows org admins to update' do
    expect(@iaas_configuration.authorizer).to be_updatable_by(@admin_user)
    expect(@iaas_configuration.authorizer).to_not be_updatable_by(@admin_user_other)
    expect(@iaas_configuration.authorizer).to_not be_updatable_by(@reader_user)
    expect(@iaas_configuration.authorizer).to_not be_updatable_by(@reader_user_other)
  end
end
