# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::DockerCommandMachineSummariesController, type: :controller do
  let(:org) { @_current_user.organizations.first }

  def create_results_for_assessment(assessment)
    %i[pass fail error warn info].each do |status|
      create :result, status: status, assessment: assessment
    end
  end

  describe 'GET #show' do
    let(:machine) { create :machine, organization: org }
    let(:dc) { create :docker_command, commandable: org }
    let(:assessment) { create :white_box_assessment, machine: machine, docker_command: dc }
    let(:summary) { DockerCommandMachineSummary.new(docker_command: dc) }

    before :each do
      create_results_for_assessment(assessment)
    end

    def show
      norad_get :show, docker_command_id: dc.to_param
    end

    it 'calls the authorizer' do
      allow(DockerCommandMachineSummary).to receive(:new).and_return(summary)
      expect(subject).to receive(:authorize_action_for).with(summary, in: org)
      show
    end

    context 'response object' do
      before(:each) { show }

      it 'responds with success' do
        expect(response).to have_http_status :success
        expect(response).to match_response_schema('docker_command_machine_summary')
      end

      it 'includes results_summary' do
        expect(response_body['response']['machines'].first['results_summary']).to be_present
      end

      it 'includes assessment_states' do
        expect(response_body['response']['machines'].first['assessment_states']).to be_present
      end
    end
  end
end
