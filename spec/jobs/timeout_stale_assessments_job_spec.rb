# frozen_string_literal: true

require 'rails_helper'
require 'jobs/shared_examples/recurring_job.rb'

RSpec.describe TimeoutStaleAssessmentsJob, type: :job, with_resque_doubled: true do
  before :each do
    allow(@resque_module_double).to receive(:logger)
  end

  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  subject(:job) { described_class.perform }

  it_behaves_like 'a Recurring Job'

  it 'looks for stale Assessments to time out' do
    expect(Assessment).to receive(:stale).and_call_original
    perform_enqueued_jobs { job }
  end

  it 'sends the timeout! message to the return value of Assessment.stale' do
    assessment = double('A stale assessment')
    expect(Assessment).to receive(:stale).and_return([assessment])
    expect(assessment).to receive(:timeout!)
    perform_enqueued_jobs { job }
  end
end
