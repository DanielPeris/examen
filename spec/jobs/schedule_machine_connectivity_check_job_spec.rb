# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ScheduleMachineConnectivityCheckJob, type: :job do
  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  before :all do
    RSpec.configure { |c| c.double_docker = false }
  end

  after :all do
    RSpec.configure { |c| c.double_docker = true }
  end

  it 'is in the swarm_scheduling queue' do
    expect(described_class.new.queue_name).to eq('swarm_scheduling')
  end

  context 'when queuing checks' do
    let(:check) { create :ping_connectivity_check }
    subject(:job) { described_class.perform_later(check.id, {}) }

    it 'queues with proper arguments' do
      expect(DockerSwarm)
        .to receive(:queue_machine_connectivity_container).with(check, {})
      perform_enqueued_jobs { job }
    end
  end
end
