# frozen_string_literal: true

require 'rails_helper'
require 'jobs/shared_examples/recurring_job.rb'

RSpec.describe SyncRelayVersionJob, type: :job, with_resque_doubled: true do
  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  subject(:job) { described_class.perform }

  it_behaves_like 'a Recurring Job'

  it 'calls RelayImageInfoFetcher#fetch_and_cache_latest_version' do
    expect(RelayImageInfoFetcher).to receive(:fetch_and_cache_latest_version).once
    perform_enqueued_jobs { job }
  end
end
