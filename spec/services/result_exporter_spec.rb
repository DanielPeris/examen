# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ResultExporter do
  describe 'instance methods' do
    let(:exporter) { described_class.new(export_queue, scan) }
    let(:export_queue) { double }
    let(:scan) { double }

    describe '#publish' do
      before :each do
        allow(exporter).to receive(:results_data).and_return({})
        allow(export_queue).to receive(:amqp_queue_name).and_return('fooqueue')
      end

      it 'asks the Publisher service to publish to the default exchange' do
        expect(Publisher).to receive(:publish).with('', instance_of(Hash), instance_of(String)).and_return(true)
        exporter.publish
      end

      it 'raises error when Publisher cannot publish to queue' do
        expect(Publisher).to receive(:publish).with('', instance_of(Hash), instance_of(String)).and_return(false)
        expect { exporter.publish }.to raise_error(/^Error publishing results to/)
      end
    end

    describe '#results_data' do
      before :each do
        allow(exporter).to receive(:results).and_return([double, double])
      end

      it 'builds a hash with serialized result data' do
        expect(ResultExporterSerializers::Result).to receive(:new).exactly(2).times
        exporter.results_data
      end
    end
  end
end

RSpec.describe ResultExporterSerializers::Result do
  let(:result) { create :result }
  let(:serializer) { described_class.new(result) }
  let(:whole_host_result) { create :result, assessment: (create :whole_host_assessment) }
  let(:whole_host_serializer) { described_class.new(whole_host_result) }

  context '#data' do
    it 'uses ::ResultSerializer for data serialization' do
      expect(::ResultSerializer).to receive(:new).with(instance_of(Result))
      serializer.data
    end
  end

  context '#meta' do
    it 'returns hash of meta data related to result' do
      expect(serializer.meta).to be_a(Hash)
      expect(serializer.meta.keys).to include(:assessment, :organization, :machine, :service, :scan_id)
    end
  end

  context '#serialized_service' do
    it 'returns an empty json object for nil service' do
      expect(whole_host_serializer.meta[:service]).to eql({})
    end

    it 'returns a serialized service' do
      expect(serializer.meta[:service].to_h.keys).to include(:id, :name, :port, :port_type, :encryption_type, :type)
    end
  end
end
