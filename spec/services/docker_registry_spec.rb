# frozen_string_literal: true

require 'rails_helper'

# FIXME: Revisit testing this more thoroughly when we have HTTP request playback
# set up.
RSpec.describe DockerRegistry do
  let(:json) { { 'test_data' => 'valid' } }
  let(:auth_header) do
    { 'www-authenticate' => 'Bearer realm="https://norad-registry.cisco.com:5001/auth",service="Docker Registry",'\
      'scope="repository:relay:pull"' }
  end

  before :each do
    @io_double = double
    allow(@io_double).to receive(:read).and_return json.to_json
    allow(@io_double).to receive(:status).and_return(%w[401 Unauthorized])
    allow(@io_double).to receive(:meta).and_return auth_header
  end

  context 'class methods' do
    describe '#json_request' do
      it 'fetches json without errors' do
        allow(described_class).to receive(:open).and_return @io_double
        expect(described_class.json_request('irrelevant')).to eq json
      end
    end
  end
end
