# frozen_string_literal: true

require 'database_cleaner'
require 'factory_bot_rails'
require 'shoulda-matchers'

# Code coverage reports
require 'simplecov'
SimpleCov.start do
  add_filter '/spec/'
  add_filter '/db/'
  add_filter '/config/'
  add_filter '/db/'

  add_group 'Models', 'app/models'
  add_group 'Controllers', 'app/controllers'
  add_group 'Query Objects', 'app/query_objects'
  add_group 'Service Objects', 'app/services'
  add_group 'Authorizers', 'app/authorizers'
  add_group 'Jobs', 'app/jobs'
  add_group 'Mailers', 'app/mailers'
  add_group 'State Machines', 'app/state_machines'
  add_group 'Libraries', 'lib/'

  track_files '{app,lib}/**/*.rb'
end

# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
RSpec.configure do |config|
  config.add_setting :double_docker, default: true
  config.add_setting :double_relay_image_info_fetcher, default: true
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.include FactoryBot::Syntax::Methods

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with :truncation
  end

  config.before(:each, type: :controller) do
    @_current_user = create :user
    allow(controller).to receive(:current_user).and_return(@_current_user)
    allow(controller).to receive(:require_api_token).and_return(true)
  end

  config.before(:each, with_after_commit: true) do
    DatabaseCleaner.strategy = :truncation
  end

  config.after(:each, with_after_commit: true) do
    DatabaseCleaner.strategy = :transaction
  end

  config.before(:each, with_resque_doubled: true) do
    @resque_module_double = object_double('Resque').as_stubbed_const
    allow(@resque_module_double).to receive(:set_schedule)
    allow(@resque_module_double).to receive(:remove_schedule)
    allow(@resque_module_double).to receive_message_chain(:logger, :error)
    allow(@resque_module_double).to receive_message_chain(:logger, :info)
  end

  config.before(:each) do
    @airbrake_module_double = object_double('Airbrake').as_stubbed_const
    allow(@airbrake_module_double).to receive(:notify_sync)
  end

  config.before(:each) do
    if RSpec.configuration.double_docker
      @docker_swarm_double = object_double('DockerSwarm').as_stubbed_const
      @schedule_machine_connectivity_check_job = object_double('ScheduleMachineConnectivityCheckJob').as_stubbed_const
      allow(@schedule_machine_connectivity_check_job).to receive(:perform_later)
      allow(@docker_swarm_double).to receive(:queue_machine_connectivity_container)
      allow(@docker_swarm_double).to receive(:queue_container)
      allow(@docker_swarm_double).to receive(:queue_discovery_container)
    end
  end

  config.before(:each) do
    if RSpec.configuration.double_relay_image_info_fetcher
      @relay_image_info_fetcher_double = object_double('RelayImageInfoFetcher').as_stubbed_const
      allow(@relay_image_info_fetcher_double).to receive(:cached_latest_version)
    end
  end

  config.before(:each) do
    Rails.cache.clear
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :active_record
    with.library :active_model
  end
end
