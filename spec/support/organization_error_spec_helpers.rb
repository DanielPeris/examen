# frozen_string_literal: true

module OrganizationErrorSpecHelpers
  def described_errors(org)
    org.organization_errors.where(type: described_class.to_s).reload
  end
end
