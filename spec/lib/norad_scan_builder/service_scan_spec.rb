# frozen_string_literal: true

require 'rails_helper'
require './lib/norad_scan_builder'
require 'lib/norad_scan_builder/shared_examples/norad_scan'

RSpec.describe NoradScanBuilder::ServiceScan, with_resque_doubled: true do
  it_should_behave_like 'a Norad Scan' do
    let(:args_klass) { NoradScanBuilder::ServiceScanArgs }
    let(:target_klass) { NoradScanBuilder::ServiceScanTarget }
    let(:options) { { service: double, machine: double } }
  end
end
