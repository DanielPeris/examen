# frozen_string_literal: true

require 'rails_helper'
require './lib/norad_scan_builder'
require 'lib/norad_scan_builder/shared_examples/scan_args'

RSpec.describe NoradScanBuilder::HostScanArgs, with_resque_doubled: true do
  it_should_behave_like 'a Scan Args class' do
    let(:options) { { machine: build_stubbed(:machine) } }
  end

  describe '#to_a' do
    let(:ip) { "192.168.1.#{rand(1..255)}" }
    let(:machine) { create(:machine, ip: ip, fqdn: nil) }
    let(:container) { create(:machine_test) }
    let(:scan_args) { described_class.new(container, machine: machine) }

    it 'uses the machine for the target' do
      expect(scan_args.to_a).to include(machine.ip)
    end

    context 'for a configurable test' do
      let(:default_foo) { 'bar' }
      let(:prog_args) { '%{target} -x %{foo}' }
      let(:container) do
        create(:security_container, configurable: true, default_config: { foo: default_foo }, prog_args: prog_args)
      end

      it 'uses the machine test config if it exists' do
        create(:security_container_config,
               configurable: machine,
               values: { 'foo' => 'baz' },
               security_container: container)

        expect(scan_args.to_a).to include('baz')
        expect(scan_args.to_a).to_not include(default_foo)
      end

      it 'uses the default config if no machine test config exists' do
        expect(scan_args.to_a).to include(default_foo)
      end
    end
  end
end
