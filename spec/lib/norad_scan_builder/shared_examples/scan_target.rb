# frozen_string_literal: true

#
require 'rails_helper'
require './lib/norad_scan_builder'

RSpec.shared_examples 'a Scan Target' do
  let(:options) { {} }
  let(:secret_id) { 1 }
  let(:container) { build_stubbed(:security_container) }
  let(:command) { build_stubbed(:docker_command) }
  let(:assessments) { double }
  let(:assessment) { build_stubbed(:assessment) }
  let(:machine) { build_stubbed(:machine) }

  describe 'instantion' do
    it 'requires the options to be present' do
      options.keys.each do |opt|
        expect do
          # Hash#except is provided by ActiveSupport, not available in vanilla Ruby
          described_class.new(container, secret_id, command, machine, options.except(opt))
        end.to raise_exception(KeyError)
      end
    end
  end

  describe 'creating an assessment object in the db' do
    before(:each) do
      allow(command).to receive(:assessments).and_return(assessments)
    end

    it 'uses the create! method to create the assessment' do
      expect(assessments).to receive(:create!)
      described_class.new(container, secret_id, command, machine, options)
    end
  end

  describe 'assessment creation error handling' do
    let(:generator) { ResultGenerator.new(assessment) }

    before(:each) do
      allow(command).to receive(:assessments).and_return(assessments)
      allow(assessments).to receive(:create!).and_return(assessment)
      allow(ResultGenerator).to receive(:new).and_return(generator)
    end

    it 'handles the assessment not being created at all' do
      allow(assessments).to receive(:create!).and_raise('oops')
      expect(generator).to_not receive(:create_errored_api_result!)
      target = described_class.new(container, secret_id, command, machine, options)
      expect(target.to_h[:id]).to be_nil
    end

    it 'cancels the assessment if an exception occurs when obtaining the target address' do
      allow(machine).to receive(:target_address).and_raise('oops')
      expect(generator).to receive(:create_errored_api_result!)
      target = described_class.new(container, secret_id, command, machine, options)
      expect(target.to_h[:id]).to be_nil
    end

    it 'cancels the assessment if an exception occurs when obtaining the results_path' do
      allow(assessment).to receive(:results_path).and_raise('oops')
      expect(generator).to receive(:create_errored_api_result!)
      target = described_class.new(container, secret_id, command, machine, options)
      expect(target.to_h[:id]).to be_nil
    end
  end
end
