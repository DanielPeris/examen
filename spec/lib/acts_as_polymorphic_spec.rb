# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ActsAsPolymorphic, type: :concern do
  before :all do
    ActiveRecord::Schema.define do
      create_table :poly_commands, force: true do |t|
        t.references :poly_machine
        t.references :poly_organization
        t.references :poly_repo
      end

      create_table :poly_machines, force: true do |t|
      end

      create_table :poly_organizations, force: true do |t|
      end

      create_table :poly_repos, force: true do |t|
      end
    end
  end

  after :all do
    ActiveRecord::Schema.define do
      drop_table :poly_commands
      drop_table :poly_machines
      drop_table :poly_organizations
      drop_table :poly_repos
    end
  end

  class PolyCommand < ApplicationRecord
    acts_as_poly :commandable, :poly_machine, :poly_organization, :poly_repo
  end

  class PolyMachine < ApplicationRecord
  end

  class PolyOrganization < ApplicationRecord
  end

  class PolyRepo < ApplicationRecord
  end

  class PolyNoAr
  end

  before :each do
    @pc = PolyCommand.new
  end

  it 'adds the acts_as_poly method to ActiveRecord classes' do
    expect(PolyCommand.respond_to?(:acts_as_poly)).to eq(true)
    expect(PolyNoAr.respond_to?(:acts_as_poly)).to eq(false)
  end

  it 'sets up the belongs_to relationship for the poly associations' do
    expect(@pc.respond_to?(:poly_machine)).to eq(true)
    expect(@pc.respond_to?(:poly_organization)).to eq(true)
    expect(@pc.respond_to?(:poly_repo)).to eq(true)
  end

  it 'enforces that only one belongs_to relationship can be associated at a time' do
    @pc.poly_machine = PolyMachine.create
    @pc.poly_organization = PolyOrganization.create
    @pc.valid?
    expect(@pc.errors.messages[:commandable]).to include('only one column for the association can be populated')
    @pc.poly_organization = nil
    @pc.valid?
    expect(@pc.errors.messages[:commandable]).to eq([])
  end

  it 'provides a getter which returns the associated object' do
    @pc.poly_repo = PolyRepo.create
    @pc.save!
    expect(@pc.commandable).to eq(PolyRepo.first)
  end

  it 'provides a setter for setting the associated object' do
    @pc.commandable = PolyMachine.create
    @pc.save!
    expect(@pc.commandable).to eq(PolyMachine.first)
  end

  it 'raises a TypeMismatch error if you try to assign nonsense' do
    expect { @pc.commandable = [] }.to raise_error(ActiveRecord::AssociationTypeMismatch)
  end

  it 'allows you to nil the association' do
    @pc.commandable = PolyMachine.create
    @pc.save!
    expect(@pc.commandable).to eq(PolyMachine.first)
    @pc.commandable = nil
    @pc.save!
    expect(@pc.commandable).to be(nil)
  end
end
