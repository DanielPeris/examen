# frozen_string_literal: true

# == Schema Information
#
# Table name: scan_containers
#
#  id                    :integer          not null, primary key
#  docker_command_id     :integer          not null
#  security_container_id :integer          not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
# Indexes
#
#  index_scan_containers_on_d_c_id_and_s_c_id  (docker_command_id,security_container_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_214538d76e  (security_container_id => security_containers.id) ON DELETE => cascade
#  fk_rails_a9e9cc8192  (docker_command_id => docker_commands.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :scan_container do
    security_container
    docker_command
  end
end
