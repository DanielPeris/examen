# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_scan_schedules
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  period          :integer          default("daily"), not null
#  at              :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_organization_scan_schedules_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_129af2457b  (organization_id => organizations.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :organization_scan_schedule do
    at '24:24'
    period 'daily'
    organization
  end
end
