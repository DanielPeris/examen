# frozen_string_literal: true

# == Schema Information
#
# Table name: docker_commands
#
#  id                    :integer          not null, primary key
#  error_details         :text
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  machine_id            :integer
#  organization_id       :integer
#  state                 :integer          default("pending"), not null
#  assessments_in_flight :integer          default(0)
#  started_at            :datetime
#  finished_at           :datetime
#
# Indexes
#
#  index_docker_commands_on_machine_id       (machine_id)
#  index_docker_commands_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_0545a1ce1d  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_b85f190c31  (machine_id => machines.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :docker_command do
    association :commandable, factory: :machine

    after(:build) do |dc|
      if dc.scan_containers.blank?
        sc = create(:security_container)
        dc.scan_containers.build(security_container: sc)
      end
    end
  end

  factory :docker_command_from_unofficial_repository, class: DockerCommand do
    after(:build) do |dc|
      sc = create(:private_container)
      create(:repository_whitelist_entry,
             organization: dc.resolved_organization,
             security_test_repository: sc.security_test_repository)
      dc.scan_containers.build(security_container: sc)
    end
  end
end
