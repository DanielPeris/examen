# frozen_string_literal: true

# == Schema Information
#
# Table name: machine_connectivity_checks
#
#  id                           :integer          not null, primary key
#  machine_id                   :integer          not null
#  security_container_secret_id :integer
#  finished_at                  :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  status                       :integer
#  type                         :string           not null
#
# Indexes
#
#  index_machine_connectivity_checks_on_finished_at          (finished_at)
#  index_machine_connectivity_checks_on_machine_id_and_type  (machine_id,type)
#  index_machine_connectivity_checks_on_s_c_s_id             (security_container_secret_id) UNIQUE
#  index_machine_connectivity_checks_on_status               (status)
#  index_machine_connectivity_checks_on_updated_at           (updated_at)
#
# Foreign Keys
#
#  fk_rails_74009af901  (machine_id => machines.id) ON DELETE => cascade
#  fk_rails_7a47e669da  (security_container_secret_id => security_container_secrets.id) ON DELETE => nullify
#

FactoryBot.define do
  factory :machine_connectivity_check do
    machine
    security_container_secret
    type 'MachineConnectivityCheck'

    factory :ping_connectivity_check, class: PingConnectivityCheck do
      type 'PingConnectivityCheck'

      factory :expired_machine_connectivity_check do
        updated_at { 25.hours.ago }
      end

      factory :stale_machine_connectivity_check do
        updated_at { 11.minutes.ago }
      end

      factory :finished_machine_connectivity_check do
        updated_at { 1.hour.ago }
        created_at { 65.minutes.ago }
        finished_at { 1.hour.ago }
        status 'passing'
      end
    end

    factory :ssh_connectivity_check, class: SshConnectivityCheck do
      type 'SshConnectivityCheck'
      machine { create(:ssh_machine) }
    end
  end
end
