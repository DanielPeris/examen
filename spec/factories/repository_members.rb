# frozen_string_literal: true

# == Schema Information
#
# Table name: repository_members
#
#  id                          :integer          not null, primary key
#  user_id                     :integer          not null
#  security_test_repository_id :integer          not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_repository_members_on_security_test_repository_id  (security_test_repository_id)
#  index_repository_members_on_u_id_and_s_t_r_id            (user_id,security_test_repository_id) UNIQUE
#  index_repository_members_on_user_id                      (user_id)
#
# Foreign Keys
#
#  fk_rails_2a2cfda3a3  (user_id => users.id) ON DELETE => cascade
#  fk_rails_e2d371a88f  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :repository_member do
    user
    security_test_repository

    factory :repository_admin do
      role_type :admin
    end

    factory :repository_reader do
      role_type :reader
    end
  end
end
