# frozen_string_literal: true

# == Schema Information
#
# Table name: repository_whitelist_entries
#
#  id                          :integer          not null, primary key
#  organization_id             :integer          not null
#  security_test_repository_id :integer          not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_r_w_entries_on_o_id_and_s_t_r_id  (organization_id,security_test_repository_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_6d4fcfc98d  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#  fk_rails_daf54902a6  (organization_id => organizations.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :repository_whitelist_entry do
    organization
    security_test_repository
  end
end
