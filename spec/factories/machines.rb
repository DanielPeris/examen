# frozen_string_literal: true

# == Schema Information
#
# Table name: machines
#
#  id                 :integer          not null, primary key
#  organization_id    :integer
#  ip                 :inet
#  fqdn               :string
#  description        :text
#  machine_status     :integer          default("pending"), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  name               :string           not null
#  use_fqdn_as_target :boolean          default(FALSE), not null
#
# Indexes
#
#  index_machines_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_machines_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_bd87ec17a7  (organization_id => organizations.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :machine do
    organization
    # 2_904_920_404 corresponds to '173.37.145.84' (cisco.com). Hopefully a good
    # starting point. Point is, to not get any RFC1918 IP Addresses because for local
    # addresses we require the relay to be configured, and an arbitrary new machine
    # might not have that.
    sequence(:ip) { |n| IPAddr.new(2_904_920_404 + n, Socket::AF_INET).to_s }
    sequence(:fqdn) { |n| "factory.server.local#{n}" }
    description 'Factory machine description'
    name { SecureRandom.uuid }

    factory :ssh_machine do
      after(:build) do |machine|
        machine.ssh_key_pair = create(:ssh_key_pair)
        machine.services << create(:ssh_service, machine: machine)
      end
    end
  end
end
