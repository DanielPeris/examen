# frozen_string_literal: true

# == Schema Information
#
# Table name: ssh_key_pairs
#
#  id                 :integer          not null, primary key
#  name               :string           not null
#  description        :text
#  username_encrypted :string
#  key_encrypted      :string
#  key_signature      :string           not null
#  organization_id    :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_ssh_key_pairs_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_ssh_key_pairs_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_3811d4539d  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'
require 'models/shared_examples/organization_error_watcher'

RSpec.describe SshKeyPair, type: :model do
  let(:dsa_key) do
    <<~EOS
      -----BEGIN DSA PRIVATE KEY-----
      MIIBuwIBAAKBgQC/wqSjWY/9M08Cl4cgCNghJLOuCRdo6+VYduDabZGC7TcOQEu8
      puLSGHgjr2+fZhpAiHuuYS+LxQn7XKlQ5vzRuqif8VBBveZrKWXOVjjxQicrzh0w
      fNy819nXhJCHOzziPabz1AA4fQFVo1zM/0EvR54EuZ2yxFFRl80r2bqLiwIVALYk
      4+1xw/9hbJIazdeR1kvXpx2rAoGAThEskeH1LLH7pyCU/m6dVvIYLXgumUcseJfT
      E8kJG6wQaP/2CNMQWf/TDdpc+c3AZFYGjTikq0DAwxYH7VyNbB211gVwaig7ZCy5
      CcwH854CQEVPj2BDlyzgOvTmXE70ePT7Gc6mxxhJ7+li0WFKarS/ITykpQep78xK
      nOCt6EUCgYBztnj6ooNrbUZWVjV3a//6lQSAP9SulN5jfFuPKhTGEfHNv0j9KrPA
      wCEfyKQAxQqYTIhAgF6DnoEIBtCbTGa33rUsQgNdC33pzBG0Z2GKtU6qOWh2mqqA
      X6zPmBcBYhHXn4IZd385ypI/kcptPs89dI/v2/ffZvnpAGdgTYjQqgIVAIQJ8h0Q
      1gS8sZhS6ccdGdxa03Vj
      -----END DSA PRIVATE KEY-----
    EOS
  end

  let(:rsa_key) do
    <<~EOS
      -----BEGIN RSA PRIVATE KEY-----
      MIIByQIBAAJhAJRUM0EuLx/GSt9c9rG5M+TJg/RrU2pOcKx78bGeoqRdpUKr2Rmc
      a9MpqFMJyrtXzwWyk9h5yD6LBwqodylikbI5RvZIrq8l7zEnL2uWS0nvGDe9aamw
      zA2eJgRb/Zt9DwIDAQABAmAqHOncdK9edx8k4bEM7odESs6TI7GwOgsVfa82fdKx
      lgDot//QnK+kaWxn/xy3KgunbIegtVZjdE62fOa9puIiZJ1p4PnUqir1ruKNtvaI
      PBc0x1wvnIvWhfPFhvr2CqECMQDDGwPdV6Olc3oyHw0j62FVVhXVW5sazuNilD/I
      +FPRcJ/bksPsMxliy2HJeexSK1ECMQDCn7hpyqOaeNUic5OeZNMXfoZ96a6j2Hmr
      MJo+QdaEVNFdMM/Fp0FlAMUlLQsYSl8CMEe3dHI2NscobXBjDZd4fCy8GgZ1R8xQ
      5DBmQhQDg/vmpDw39KCsH9a639UJZh7GIQIwYIzb75+Xigpnsa+ki+94yS77iXtk
      J+Q8d7Ck1D3VLdDmDFUqveM7jd/T7Z1q46IBAjA/D5iZcO2w9vt9N2ni7b6+RiBW
      nonoKiz0QtV3xuxahLbs9gWvMC49x2mjbd/WP2c=
      -----END RSA PRIVATE KEY-----
    EOS
  end

  let(:ec_key) do
    <<~EOS
      -----BEGIN EC PRIVATE KEY-----
      MHcCAQEEIFjQu3DNXoJI72yWcCvYIcf4lxh3fi6KioRJAIik6005oAoGCCqGSM49
      AwEHoUQDQgAE8UsDgBcWf4UhpnMvMirm+XXBmfhJb+zai2qTvo6dl4f3hQMvjTo2
      yT4VrPjGRCFk/JHmJFwueqcDNOvjGS417Q==
      -----END EC PRIVATE KEY-----
    EOS
  end

  let(:rsa_key_p) do
    <<~EOS
      -----BEGIN EC PRIVATE KEY-----
      MIIByQIBAAJhAJRUM0EuLx/GSt9c9rG5M+TJg/RrU2pOcKx78bGeoqRdpUKr2Rmca9MpqFMJyrtXzwWyk9h5yD6LBwqodylikbI5RvZIrq8l7zEnL2uWS0nvGDe9aamwzA2eJgRb/Zt9DwIDAQABAmAqHOncdK9edx8k4bEM7odESs6TI7GwOgsVfa82fdKxlgDot//QnK+kaWxn/xy3KgunbIegtVZjdE62fOa9puIiZJ1p4PnUqir1ruKNtvaIPBc0x1wvnIvWhfPFhvr2CqECMQDDGwPdV6Olc3oyHw0j62FVVhXVW5sazuNilD/I+FPRcJ/bksPsMxliy2HJeexSK1ECMQDCn7hpyqOaeNUic5OeZNMXfoZ96a6j2HmrMJo+QdaEVNFdMM/Fp0FlAMUlLQsYSl8CMEe3dHI2NscobXBjDZd4fCy8GgZ1R8xQ5DBmQhQDg/vmpDw39KCsH9a639UJZh7GIQIwYIzb75+Xigpnsa+ki+94yS77iXtkJ+Q8d7Ck1D3VLdDmDFUqveM7jd/T7Z1q46IBAjA/D5iZcO2w9vt9N2ni7b6+RiBWnonoKiz0QtV3xuxahLbs9gWvMC49x2mjbd/WP2c=
      -----END EC PRIVATE KEY-----
    EOS
  end

  context 'when validating' do
    define_method :strip_ssh_key do |key_string|
      key_string.split("\n").map(&:strip).join("\n")
    end

    it { should validate_presence_of(:organization) }
    it { should validate_presence_of(:username) }
    it { should validate_presence_of(:key) }
    it { should validate_presence_of(:name) }

    subject { build(:ssh_key_pair, name: "keypair_name_#{rand}") }
    it { should validate_uniqueness_of(:name).scoped_to(:organization_id) }

    it 'validates that encoded key is a valid DSA key' do
      key = build(:ssh_key_pair, key: Base64.strict_encode64(strip_ssh_key(dsa_key)))
      key.valid?
      expect(key.errors.messages[:key]).to eq([])
    end

    it 'validates that encoded key is a valid RSA key' do
      key = build(:ssh_key_pair, key: Base64.strict_encode64(strip_ssh_key(rsa_key)))
      key.valid?
      expect(key.errors.messages[:key]).to eq([])
    end

    it 'validates that encoded key is a valid EC key' do
      key = build(:ssh_key_pair, key: Base64.strict_encode64(strip_ssh_key(ec_key)))
      key.valid?
      expect(key.errors.messages[:key]).to eq([])
    end

    it 'validates that key does not have passphrase' do
      rsa_passphrase_key = '-----BEGIN RSA PRIVATE KEY-----
      Proc-Type: 4,ENCRYPTED
      DEK-Info: AES-128-CBC,E00D5E4672BB1D13A98FE87C7C57CFF9

      b2Jw4ark7Q51b1P4zKeevZsnareCeoLprbJ0eRU8MIpsYVQ5p95YQKRmOAXl9x86
      zay40EdNonTdEverX2myp8PMtowCXI9bTx1zepv8/G/G/PMR6eEOwlNN9mF8KCko
      Czr8uvnLqvgWCyr5klfWKtF/oGUHQ+R4BMs+iY4Eotz3/UAH6gbNQxAYqvJmUKu2
      4UhHjaMpqahnF9y4YqIIty6tXPUAEwN8RI/8YiMjlELgZA+wxFDmTX9qu+LJWSx5
      Jyq6L5k811QBGiB/pnPiFl5uapREhuhJh9kcnQSpesw53J7qo/CiQx9VF2BSh4G0
      9JGBy/Xo7IlZIIL0gF+OXo75aBlt9QRV3wEOIVqGlhG/EqPzBxK5a9QA3EtxRuEf
      pV1nLbADltBzWLozTbPIHf1UXE6BRrIcUI0pyn2iTidFooiQITONkoddfVZAL4Xi
      YMn3+OoHpOmuHpcxqgoCQTOjYCwhy0ngZ5PPDVlWV4wVWCvNR2j7CcupWlXAcHTM
      cHi0T8n9FvVw8dyeH6pnsEfvj3hEyOzsJDp2qsIGCPnhAEKvXf/t7UwAYLKGiulb
      GzrBOKcJf+3o+zV76n0jVeT5aIS+Ql7hw8ocSyEIpvc=
      -----END RSA PRIVATE KEY-----'

      key = build(:ssh_key_pair, key: Base64.strict_encode64(strip_ssh_key(rsa_passphrase_key)))
      key.valid?
      expect(key.errors.messages[:key]).to include(
        'must be Base64 encoded and in a valid RSA/DSA format with no passphrase'
      )
    end

    it 'validates that key is Base64 encoded' do
      key = build(:ssh_key_pair, key: OpenSSL::PKey::RSA.new(2048))
      key.valid?
      expect(key.errors.messages[:key]).to include(
        'must be Base64 encoded and in a valid RSA/DSA format with no passphrase'
      )
    end
  end

  context 'when maintaining associations' do
    it { should belong_to(:organization) }
    it { should have_many(:ssh_key_pair_assignments) }
    it { should have_many(:machines).through(:ssh_key_pair_assignments) }
  end

  context 'when executing callbacks' do
    context 'before validation' do
      it 'removes newlines from the key' do
        key = build(:ssh_key_pair, key: Base64.encode64("NOT A\n KEY!!"))
        key.valid?
        expect(key.key).to_not match(/\n+/)
      end
    end

    context 'before creation' do
      shared_examples 'key massaging' do
        it 'adds the PEM header and footer to the key if it was not provided' do
          key = create(:ssh_key_pair, key: key_text.gsub(/^-.*$/, ''))
          pem = Base64.decode64(key.key)
          expect(pem.split("\n").first).to eq(header)
          expect(pem.split("\n").last).to eq(footer)
        end

        it 'does not include newlines in base64 encoding of the key' do
          expect(key_text).to include("\n")
          key = create(:ssh_key_pair, key: Base64.encode64(key_text))
          expect(key.key.rstrip).to_not include("\n")
        end

        it 'generates the key signature' do
          key = build :ssh_key_pair, key: Base64.encode64(key_text)
          expect(key.key_signature).to be(nil)
          key.save!
          expect(key.key_signature).to_not be(nil)
        end
      end

      context 'for DSA keys' do
        include_examples 'key massaging' do
          let(:key_text) { dsa_key }
          let(:header) { '-----BEGIN DSA PRIVATE KEY-----' }
          let(:footer) { '-----END DSA PRIVATE KEY-----' }
        end
      end

      context 'for RSA keys' do
        include_examples 'key massaging' do
          let(:key_text) { rsa_key }
          let(:header) { '-----BEGIN RSA PRIVATE KEY-----' }
          let(:footer) { '-----END RSA PRIVATE KEY-----' }
        end
      end

      context 'for EC keys' do
        include_examples 'key massaging' do
          let(:key_text) { ec_key }
          let(:header) { '-----BEGIN EC PRIVATE KEY-----' }
          let(:footer) { '-----END EC PRIVATE KEY-----' }
        end
      end
    end

    context 'after saving' do
      it 'checks for SSH Key Pair Errors after creation' do
        ssh_key_pair = build :ssh_key_pair
        expect(NoSshKeyPairError).to receive(:check).with(ssh_key_pair.organization, subject: ssh_key_pair)
        ssh_key_pair.save!
      end

      it 'checks for SSH Key Pair Errors after updating' do
        ssh_key_pair = create :ssh_key_pair
        expect(NoSshKeyPairError).to receive(:check).with(ssh_key_pair.organization, subject: ssh_key_pair)
        ssh_key_pair.name = SecureRandom.hex
        ssh_key_pair.save!
      end
    end

    context 'when destroying' do
      let(:ssh_key_pair) do
        # create machine as well
        create(:ssh_key_pair_assignment).ssh_key_pair
      end

      it 'checks for SSH Key Pair Errors after destroying' do
        expect(NoSshKeyPairError).to receive(:check).with(ssh_key_pair.organization, subject: ssh_key_pair)
        ssh_key_pair.destroy!
      end

      it 'removes associated UnableToSshToMachineErrors before destroying' do
        create :unable_to_ssh_to_machine_error, errable: ssh_key_pair.machines.first
        expect { ssh_key_pair.destroy! }.to change(UnableToSshToMachineError, :count).by(-1)
      end
    end
  end

  context 'when watching for organization errors' do
    it_behaves_like 'an Organization Error Watcher'
  end
end
