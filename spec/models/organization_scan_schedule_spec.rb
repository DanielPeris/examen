# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_scan_schedules
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  period          :integer          default("daily"), not null
#  at              :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_organization_scan_schedules_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_129af2457b  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'
require 'models/shared_examples/scan_schedule'

RSpec.describe OrganizationScanSchedule, type: :model do
  it_should_behave_like 'a Scan Schedule' do
    let(:schedule_type) { :organization_scan_schedule }
    let(:schedule_for) { :organization }
  end
end
