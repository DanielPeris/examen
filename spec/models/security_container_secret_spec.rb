# frozen_string_literal: true

# == Schema Information
#
# Table name: security_container_secrets
#
#  id               :integer          not null, primary key
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  secret_encrypted :string
#

require 'rails_helper'

RSpec.describe SecurityContainerSecret, type: :model do
  it { should have_many(:assessments) }
  it { should have_many(:service_discoveries) }

  context 'when executing callbacks' do
    it 'generates a secret' do
      secret = build :security_container_secret, secret: nil
      expect(secret.secret).to be nil
      secret.save!
      expect(secret.secret).to_not be nil
    end
  end

  context 'when encrypting sensitive values' do
    it 'stores the secret as an encrypted value' do
      secret = create :security_container_secret, secret: 'supersecret'
      expect(secret.secret_encrypted).to_not eq(secret.secret)
    end
  end
end
