# frozen_string_literal: true

# == Schema Information
#
# Table name: machine_connectivity_checks
#
#  id                           :integer          not null, primary key
#  machine_id                   :integer          not null
#  security_container_secret_id :integer
#  finished_at                  :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  status                       :integer
#  type                         :string           not null
#
# Indexes
#
#  index_machine_connectivity_checks_on_finished_at          (finished_at)
#  index_machine_connectivity_checks_on_machine_id_and_type  (machine_id,type)
#  index_machine_connectivity_checks_on_s_c_s_id             (security_container_secret_id) UNIQUE
#  index_machine_connectivity_checks_on_status               (status)
#  index_machine_connectivity_checks_on_updated_at           (updated_at)
#
# Foreign Keys
#
#  fk_rails_74009af901  (machine_id => machines.id) ON DELETE => cascade
#  fk_rails_7a47e669da  (security_container_secret_id => security_container_secrets.id) ON DELETE => nullify
#

require 'rails_helper'

RSpec.describe MachineConnectivityCheck, type: :model do
  it { should belong_to(:security_container_secret) }
  it { should belong_to(:machine) }

  context 'when applying scopes' do
    it 'returns expired checks' do
      expired_check = create :expired_machine_connectivity_check
      recent_check = create :machine_connectivity_check
      expect(described_class.expired).to include expired_check
      expect(described_class.expired).not_to include recent_check
    end
  end

  context 'validations' do
    it 'should check for eligibility when created' do
      check = build(:ping_connectivity_check)
      expect(check).to receive(:eligible).and_call_original
      check.save!
    end

    it 'should not check for eligibility when updating' do
      check = create :ping_connectivity_check
      expect(check).not_to receive(:eligible)
      check.update!(status: :failing)
    end

    it 'should not check for eligibility when destroying' do
      check = create :ping_connectivity_check
      expect(check).not_to receive(:eligible)
      check.destroy!
    end
  end

  context 'when running callbacks' do
    describe 'after_commit' do
      it 'should create associated org error' do
        expect do
          create :ping_connectivity_check, status: 'failing'
        end.to change(UnableToPingMachineError, :count).by(1)
      end

      it 'should remove associated org error' do
        machine = create :machine
        create :unable_to_ping_machine_error, errable: machine
        expect do
          create :ping_connectivity_check, status: 'passing', machine: machine
        end.to change(UnableToPingMachineError, :count).by(-1)
      end
    end
  end
end
