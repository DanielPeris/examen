# frozen_string_literal: true

# == Schema Information
#
# Table name: services
#
#  id                  :integer          not null, primary key
#  name                :string           not null
#  description         :text
#  port                :integer          not null
#  port_type           :integer          default("tcp"), not null
#  encryption_type     :integer          default("cleartext"), not null
#  machine_id          :integer
#  type                :string           not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  allow_brute_force   :boolean          default(FALSE), not null
#  application_type_id :integer
#  discovered          :boolean          default(FALSE), not null
#
# Indexes
#
#  index_services_on_application_type_id  (application_type_id)
#  index_services_on_machine_id           (machine_id)
#  index_services_on_machine_id_and_port  (machine_id,port) UNIQUE
#  index_services_on_type                 (type)
#
# Foreign Keys
#
#  fk_rails_6a8ba918c1  (application_type_id => application_types.id)
#  fk_rails_b32a34656d  (machine_id => machines.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe Service, type: :model do
  context 'when validating' do
    it { should validate_presence_of(:machine) }
    it { should validate_presence_of(:port) }
    it { should validate_presence_of(:name) }
    it { should validate_inclusion_of(:port).in_range(1..65_535).with_message('must be a number between 1 and 65,535') }
    it { should validate_inclusion_of(:type).in_array(Service::VALID_TYPES) }

    it 'validates port uniqueness' do
      expect(create(:service)).to validate_uniqueness_of(:port).scoped_to(:machine_id)
    end
  end

  context 'when maintaining associations' do
    it { should belong_to(:machine) }
    it { should have_one(:service_identity) }
  end

  context 'when receiving class methods' do
    describe '::build_or_update_discovered_service' do
      it 'only modifies services that were discovered' do
        new_name = SecureRandom.hex
        machine = create :machine
        s1 = create :generic_service, discovered: false, machine: machine
        attrs = attributes_for :generic_service, machine_id: nil, name: new_name, port: s1.port
        ret = machine.services.build_or_update_discovered_service(attrs)
        expect(ret.name).to eq(s1.name)
        s2 = create :generic_service, discovered: true, machine: machine
        attrs = attributes_for :generic_service, machine_id: nil, name: new_name, port: s2.port
        ret = machine.services.build_or_update_discovered_service(attrs)
        expect(ret.name).to eq(new_name)
      end

      it 'sets the discovered attribute to true' do
        machine = create :machine
        attrs = attributes_for :service, machine_id: nil
        ret = machine.services.build_or_update_discovered_service(attrs)
        expect(ret.discovered).to eq(true)
      end

      it 'returns the object' do
        s1 = create :generic_service
        attrs = { port: s1.port }
        expect(Service.build_or_update_discovered_service(attrs).id).to eq(s1.id)
        machine = create :machine
        attrs = attributes_for :service, machine_id: nil
        ret = machine.services.build_or_update_discovered_service(attrs)
        expect(ret.port).to eq(attrs[:port])
      end
    end
  end

  context 'after saving' do
    let(:service) { build :service }

    it 'checks for Machine Connectivity Errors after Service is created' do
      expect(UnableToPingMachineError).to receive(:check).with(service.machine.organization, subject: service.machine)
      service.save!
    end

    it 'checks for Machine Connectivity Errors after Service is updated' do
      expect(UnableToPingMachineError).to receive(:check).with(service.machine.organization, subject: service.machine)
      service.port_type = :udp
      service.save!
    end
  end

  context 'after destroying' do
    let!(:service) { create :service }

    it 'checks for Machine Connectivity Errors after Service is destroyed' do
      expect(UnableToPingMachineError).to receive(:check).with(service.machine.organization, subject: service.machine)
      service.destroy
    end
  end
end
