# frozen_string_literal: true

require 'support/organization_error_spec_helpers'

RSpec.shared_examples 'a Machine Error class' do
  include ActiveJob::TestHelper
  include OrganizationErrorSpecHelpers

  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  before :each do
    @organization = create :organization
    @machine = create :machine, organization: @organization, ip: '8.8.8.8'
    create :ssh_key_pair_assignment, machine: @machine
    create :ssh_service, machine: @machine
    allow_any_instance_of(DockerRelay).to receive(:check_for_organization_errors)
    allow_any_instance_of(Machine).to receive(:check_for_organization_errors)
  end

  context 'when machine is destroyed' do
    before :each do
      @error = create described_class.to_s.underscore, errable: @machine.reload
    end

    it 'removes associated errors' do
      expect { @machine.destroy }.to change(OrganizationError, :count).by(-1)
      expect { @error.reload }.to raise_exception ActiveRecord::RecordNotFound
    end
  end

  describe 'the #message instance method' do
    it 'returns a customized message including machine info' do
      an_instance = described_class.new
      an_instance.errable = @machine.reload
      expect(an_instance.message).to include(@machine.name)
    end
  end
end
