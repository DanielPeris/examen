# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe MachineError, type: :model do
  context 'when queuing containers' do
    let(:machine) { create(:machine) }
    let(:organization_error_type) { 'UnableToPingMachineError' }
    let(:mcc) do
      create(:ping_connectivity_check, machine: machine)
    end

    it 'queries latest check when saving' do
      error = build(:machine_error, machine_connectivity_check: mcc, errable: machine)
      expect(error.machine_connectivity_check).to receive(:latest?).and_call_original
      expect { error.save }.to change(MachineError, :count).by(1)
    end

    it 'queries latest check when destroying' do
      error = create(:machine_error, machine_connectivity_check: mcc, errable: machine)
      expect(error.machine_connectivity_check).to receive(:latest?).and_call_original
      expect { error.destroy }.to change(MachineError, :count).by(-1)
    end

    it 'prevents saving machine_error when check is not latest' do
      error = build(:machine_error, machine_connectivity_check: mcc)
      create(:ping_connectivity_check, machine: machine)
      expect(error.save).to be false
      expect(error.errors[:base])
        .to include 'Aborting: newer machine connectivity check was found.'
    end

    it 'prevents destroying machine_error when check is not latest' do
      error = create(:machine_error, errable: machine, machine_connectivity_check: mcc)
      create(:ping_connectivity_check, machine: machine)
      expect { error.destroy }.not_to change(MachineError, :count)
      expect(error.errors[:base])
        .to include 'Aborting: newer machine connectivity check was found.'
    end
  end
end
