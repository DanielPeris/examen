# frozen_string_literal: true

# == Schema Information
#
# Table name: results
#
#  id                    :integer          not null, primary key
#  status                :integer          default("fail"), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  sir                   :integer          default("unevaluated"), not null
#  assessment_id         :integer
#  ignored               :boolean          default(FALSE), not null
#  signature             :string           not null
#  reported              :boolean          default(FALSE), not null
#  title_encrypted       :string
#  description_encrypted :string
#  output_encrypted      :string
#  nid_encrypted         :string
#
# Indexes
#
#  index_results_on_assessment_id  (assessment_id)
#  index_results_on_signature      (signature)
#
# Foreign Keys
#
#  fk_rails_1b008bfc36  (assessment_id => assessments.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe Result, type: :model do
  context 'when validating' do
    it { should validate_presence_of(:nid).with_message('can only contain alphanumeric, dashes, and underscores') }
    it { should define_enum_for(:sir).with(%i[unevaluated no_impact low medium high critical]) }
    it { should validate_presence_of :status }
    it { should validate_length_of(:signature).is_equal_to(64) }
    it { should allow_value(OpenSSL::Digest::SHA256.hexdigest(SecureRandom.hex)).for(:signature) }
    it { should_not allow_value(OpenSSL::Digest::SHA1.hexdigest(SecureRandom.hex)).for(:signature) }
  end

  it 'should allow unevaluated, no_impact, low, medium, high, or critical security impact ratings' do
    wba = create :white_box_assessment
    %i[unevaluated no_impact low medium high critical].each do |sir_stat|
      result = build_stubbed(:result, sir: sir_stat, assessment_id: wba.id, nid: 'serverspec:1234')
      expect(result.valid?).to be true
    end
  end

  context 'when maintaining associations' do
    it { should belong_to(:assessment) }
  end

  describe 'class methods' do
    it 'sets the ignored attribute to true via .ignore' do
      result = create :result, ignored: false
      described_class.where(id: result).ignore

      expect(result.reload.ignored).to eq(true)
    end

    it 'sets the ignored attribute to true via .ignore' do
      result = create :result, ignored: true
      described_class.where(id: result).unignore

      expect(result.reload.ignored).to eq(false)
    end

    it 'excludes ignored specs from all stats' do
      %i[pass fail warn info error].each do |status|
        create :result, status: status, ignored: false
        create :result, status: status, ignored: true
      end
      expect(described_class.stats).to match(passing: 1, warning: 1, failing: 1, erroring: 1, informing: 1)
    end

    it 'returns all results with a given signature via .with_signature' do
      signature = build_stubbed(:result).signature
      result = create :result, signature: signature
      create :result

      expect(described_class.with_signature(signature)).to match_array(Result.where(id: result))
    end
  end
end
