# frozen_string_literal: true

# == Schema Information
#
# Table name: ssh_key_pair_assignments
#
#  id              :integer          not null, primary key
#  machine_id      :integer          not null
#  ssh_key_pair_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_ssh_key_pair_assignments_on_machine_id       (machine_id) UNIQUE
#  index_ssh_key_pair_assignments_on_ssh_key_pair_id  (ssh_key_pair_id)
#
# Foreign Keys
#
#  fk_rails_05e7648bab  (ssh_key_pair_id => ssh_key_pairs.id) ON DELETE => cascade
#  fk_rails_7c3731b704  (machine_id => machines.id) ON DELETE => cascade
#

require 'rails_helper'
require 'models/shared_examples/organization_error_watcher'

RSpec.describe SshKeyPairAssignment, type: :model, with_docker_doubled: true do
  context 'when validating' do
    it { should validate_presence_of(:machine) }
    it { should validate_presence_of(:ssh_key_pair) }

    it 'requires the machine and keypair to belong to the same organization' do
      m1 = create :machine
      k1 = create :ssh_key_pair, organization: m1.organization
      m2 = create :machine
      k2 = create :ssh_key_pair, organization: m2.organization
      m1.ssh_key_pair = k2
      m2.ssh_key_pair = k1
      expect(m1.ssh_key_pair_assignment.errors.messages[:base]).to(
        include('Key Pair and Machine must belong to the same organization')
      )
      expect(m2.ssh_key_pair_assignment.errors.messages[:base]).to(
        include('Key Pair and Machine must belong to the same organization')
      )
      m1.ssh_key_pair = k1
      m2.ssh_key_pair = k2
      expect(m1.ssh_key_pair_assignment.errors.messages[:base]).to eq([])
      expect(m2.ssh_key_pair_assignment.errors.messages[:base]).to eq([])
    end
  end

  context 'when maintaining associations' do
    it { should belong_to(:ssh_key_pair) }
    it { should belong_to(:machine) }
  end

  context 'when executing callbacks' do
    context 'after saving' do
      it 'checks for SSH Key Pair Assignment Errors after creation' do
        ssh_key_pair_assignment = build :ssh_key_pair_assignment
        org = ssh_key_pair_assignment.machine.organization
        expect(NoSshKeyPairAssignmentError).to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        expect(KeyPairAssignmentWithoutSshServiceError)
          .to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        expect(SshServiceWithoutKeyPairAssignmentError)
          .to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        expect(UnableToSshToMachineError).to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        ssh_key_pair_assignment.save!
      end

      it 'checks for SSH Key Pair Assignment Errors after updating' do
        ssh_key_pair_assignment = create :ssh_key_pair_assignment
        org = ssh_key_pair_assignment.machine.organization
        ssh_key_pair = create :ssh_key_pair, organization: ssh_key_pair_assignment.machine.organization
        ssh_key_pair_assignment.ssh_key_pair_id = ssh_key_pair.id
        expect(NoSshKeyPairAssignmentError).to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        expect(KeyPairAssignmentWithoutSshServiceError)
          .to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        expect(SshServiceWithoutKeyPairAssignmentError)
          .to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        expect(UnableToSshToMachineError).to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        ssh_key_pair_assignment.save!
      end
    end

    context 'when destroying' do
      let(:ssh_key_pair_assignment) { create :ssh_key_pair_assignment }
      let(:machine) { ssh_key_pair_assignment.reload.machine }
      let(:org) { machine.organization }

      it 'checks for SSH Key Pair Assignment Errors after destroying' do
        org = ssh_key_pair_assignment.machine.organization
        expect(NoSshKeyPairAssignmentError).to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        expect(KeyPairAssignmentWithoutSshServiceError)
          .to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        expect(SshServiceWithoutKeyPairAssignmentError)
          .to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        expect(UnableToSshToMachineError).to receive(:check).with(org, subject: ssh_key_pair_assignment.machine)
        ssh_key_pair_assignment.destroy!
      end

      it 'removes associated UnableToSshToMachineErrors before destroying' do
        create :unable_to_ssh_to_machine_error, errable: machine, organization: org
        expect { ssh_key_pair_assignment.destroy! }.to change(UnableToSshToMachineError, :count).by(-1)
      end
    end
  end

  context 'when watching for organization errors' do
    it_behaves_like 'an Organization Error Watcher'
  end
end
