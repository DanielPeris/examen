# frozen_string_literal: true

# == Schema Information
#
# Table name: result_export_queues
#
#  id              :integer          not null, primary key
#  organization_id :integer          not null
#  created_by      :string           not null
#  type            :string           not null
#  auto_sync       :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_result_export_queues_on_organization_id           (organization_id)
#  index_result_export_queues_on_organization_id_and_type  (organization_id,type) UNIQUE WHERE ((type)::text = 'InfosecExportQueue'::text)
#  index_result_export_queues_on_type                      (type)
#
# Foreign Keys
#
#  fk_rails_e5fc28c9ec  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'
require 'models/shared_examples/result_export_queue'

RSpec.describe JiraExportQueue, type: :model do
  let(:jira_config) { create :custom_jira_configuration }
  let(:subject) { jira_config.jira_export_queue }

  it_behaves_like 'a Result Export Queue'

  it { should have_one(:custom_jira_configuration).with_foreign_key(:result_export_queue_id) }
  it { should accept_nested_attributes_for(:custom_jira_configuration) }
  it { should validate_presence_of(:custom_jira_configuration) }

  describe 'instance methods' do
    describe '#target_connection_settings' do
      it 'has the jira config attributes as the value for settings key' do
        expect(subject.target_connection_settings[:settings][:id]).to eq(jira_config.attributes[:id])
      end
    end
  end
end
