# frozen_string_literal: true

require 'rails_helper'

RSpec.describe NotificationMailer, type: :mailer do
  context 'when emailing' do
    before :each do
      @org_admin = create :user
      @org = @org_admin.organizations.first
      @machine = create :machine, organization: @org
    end

    context 'valid scans' do
      context 'on orgs' do
        before :each do
          sc = create(:security_container)
          @dc = create(
            :docker_command,
            commandable: @org,
            assessments: [create(:assessment, security_container: sc)],
            scan_containers_attributes: [{ security_container_id: sc.id }],
            started_at: 1.day.ago,
            finished_at: Time.now.utc
          )
        end

        it 'exists in deliveries queue' do
          expect { NotificationMailer.scan_complete(@dc.id).deliver_now } \
            .to change(ActionMailer::Base.deliveries, :count).by(1)
        end

        it 'includes members of org in the bcc field' do
          mail = NotificationMailer.scan_complete(@dc.id)
          expected_recipients = @org_admin.email
          expect(mail.bcc).to include(expected_recipients)
        end

        it 'displays event data in the email body' do
          mail = NotificationMailer.scan_complete(@dc.id)
          expect(mail.body).to include(@dc.started_at)
          expect(mail.body).to include(@dc.finished_at)
          expect(mail.body).to include('1 day')
        end

        it 'includes link to scan' do
          mail = NotificationMailer.scan_complete(@dc.id)
          expect(mail.body).to include("/organizations/#{@org.to_param}/results?scan_id=#{@dc.id}")
        end
      end

      context 'on machines' do
        before :each do
          sc = create(:security_container)
          @dc = create(
            :docker_command,
            commandable: @machine,
            assessments: [create(:assessment, security_container: sc)],
            scan_containers_attributes: [{ security_container_id: sc.id }],
            started_at: 1.day.ago,
            finished_at: Time.now.utc
          )
        end

        it 'includes link to scan' do
          mail = NotificationMailer.scan_complete(@dc.id)
          expect(mail.body).to include("/machines/#{@machine.to_param}/results?scan_id=#{@dc.id}")
        end
      end
    end

    context 'errored scans' do
      before :each do
        @dc = create(
          :docker_command,
          commandable: @org,
          error_details: 'Test error details'
        )
      end

      it 'displays scan errors' do
        mail = NotificationMailer.scan_complete(@dc.id)
        expect(mail.body).to include('Test error details')
      end
    end

    context 'empty scans' do
      context 'on orgs' do
        before :each do
          @dc = create(
            :docker_command,
            commandable: @org,
            started_at: 1.day.ago
          )
        end

        it 'includes the machines url' do
          mail = NotificationMailer.scan_complete(@dc.id)
          expect(mail.body).to include "/organizations/#{@org.to_param}/machines"
        end

        it 'notifies user no assessments were completed' do
          mail = NotificationMailer.scan_complete(@dc.id)
          expect(mail.body).to include('Your scan has finished, but no relevant assessments were completed.')
          expect(mail.body).to include(@dc.started_at)
        end
      end

      context 'on machines' do
        before :each do
          @dc = create(
            :docker_command,
            commandable: @machine,
            started_at: 1.day.ago
          )
        end

        it 'includes the services url' do
          mail = NotificationMailer.scan_complete(@dc.id)
          expect(mail.body).to include "/machines/#{@machine.to_param}/services"
        end

        it 'notifies user no assessments were completed' do
          mail = NotificationMailer.scan_complete(@dc.id)
          expect(mail.body).to include('Your scan has finished, but no relevant assessments were completed.')
          expect(mail.body).to include(@dc.started_at)
        end
      end
    end
  end
end
