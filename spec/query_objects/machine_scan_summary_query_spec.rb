# frozen_string_literal: true

require 'rails_helper'
require 'query_objects/shared_examples/query_object'
require 'query_objects/shared_examples/scan_history_limiting'

RSpec.describe MachineScanSummaryQuery do
  let(:machine) { create :machine }
  let(:instance) { described_class.new(relation: machine) }

  it_behaves_like 'a Query Object'

  describe '#assessments_summary' do
    it_behaves_like 'scan history limiting' do
      let(:commandable) { machine }
    end

    it 'reduces by state and creation time' do
      dc1 = create :docker_command, commandable: machine
      a1 = create :white_box_assessment, docker_command: dc1, state: 'complete', machine: machine
      create :white_box_assessment, docker_command: dc1, created_at: 1.day.ago, state: 'in_progress', machine: machine
      create :white_box_assessment, docker_command: dc1, created_at: 2.days.ago, machine: machine

      dc2 = create :docker_command, commandable: machine
      a2 = create :white_box_assessment, docker_command: dc2, created_at: 1.day.ago, state: 'in_progress',
                                         machine: machine
      create :white_box_assessment, docker_command: dc2, created_at: 5.days.ago, state: 'complete', machine: machine
      create :white_box_assessment, docker_command: dc2, created_at: 2.days.ago, state: 'in_progress', machine: machine

      expect(instance.assessments_summary[dc1.id][:created_at].to_i).to eq(a1.created_at.to_i)
      expect(instance.assessments_summary[dc1.id][:state]).to eq 'pending_scheduling'
      expect(instance.assessments_summary[dc2.id][:created_at].to_i).to eq(a2.created_at.to_i)
      expect(instance.assessments_summary[dc2.id][:state]).to eq 'in_progress'
    end

    context 'when no assessments exist for a docker command' do
      let!(:dc_with_no_assessments) { create :docker_command, commandable: machine }
      let!(:dc_with_assessments) { create :docker_command, commandable: machine }

      before :each do
        create :white_box_assessment, docker_command: dc_with_assessments, machine: machine
      end

      it 'includes the docker command in the summary' do
        expect(instance.assessments_summary).to have_key(dc_with_no_assessments.id)
        expect(instance.assessments_summary).to have_key(dc_with_assessments.id)
        expect(instance.assessments_summary.keys.size).to eq 2
      end
    end

    context 'when both org and machine docker commands exist for a machine' do
      let(:dc1) { create :docker_command, commandable: machine }
      let(:dc2) { create :docker_command, commandable: machine.organization }

      before :each do
        create :white_box_assessment, machine: machine, docker_command: dc1
        create :white_box_assessment, machine: machine, docker_command: dc2
      end

      it 'has two docker commands in the summary' do
        expect(instance.assessments_summary).to have_key(dc1.id)
        expect(instance.assessments_summary).to have_key(dc2.id)
        expect(instance.assessments_summary.keys.size).to eq 2
      end
    end
  end
end
