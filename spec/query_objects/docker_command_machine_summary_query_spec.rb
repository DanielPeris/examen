# frozen_string_literal: true

require 'rails_helper'
require 'query_objects/shared_examples/query_object'

RSpec.describe DockerCommandMachineSummaryQuery do
  let(:org) { create :organization }
  let(:machine) { create :machine, organization: org }
  let(:org_dc) { create :docker_command, commandable: org }
  let(:org_instance) { described_class.new(relation: org_dc) }

  it_behaves_like 'a Query Object'

  context 'for machines' do
    let(:dc) { create :docker_command, commandable: machine }
    let(:instance) { described_class.new(relation: dc) }

    describe '#results_summary' do
      it 'totals assessment states' do
        %i[pending_scheduling in_progress complete].each do |state|
          create :white_box_assessment, state: state, docker_command: dc, machine: machine
        end

        machines = instance.results_summary
        expect(machines.first[:assessment_states]).to eq(
          'pending_scheduling' => 1,
          'in_progress' => 1,
          'complete' => 1
        )
      end

      it 'includes the correct machines in the summary' do
        create :white_box_assessment, machine: machine, docker_command: dc
        expect(instance.results_summary.map { |m| m[:id] }).to eq [machine.id]
      end

      context 'no results' do
        it 'totals assessments results counts' do
          # Create 1 assessment without results, and 2 with
          create :white_box_assessment, docker_command: dc, machine: machine
          expect(instance.results_summary.first[:assessments_count]).to eq 1
          expect(instance.results_summary.first[:assessments_with_results_count]).to eq 0
        end
      end

      context 'with results do' do
        it 'totals assessments results counts' do
          create :white_box_assessment, docker_command: dc, machine: machine
          2.times do
            ass = create :white_box_assessment, docker_command: dc, machine: machine
            create :result, assessment: ass
          end

          expect(instance.results_summary.first[:assessments_count]).to eq 3
          expect(instance.results_summary.first[:assessments_with_results_count]).to eq 2
        end
      end

      it 'totals result statuses' do
        assessment = create :white_box_assessment, machine: machine, docker_command: dc
        %i[pass fail warn info error pass error].each do |status|
          create :result, status: status, assessment: assessment
        end

        machines = instance.results_summary
        expect(machines.first[:results_summary]).to eq(
          'passing' => 2,
          'erroring' => 2,
          'informing' => 1,
          'failing' => 1,
          'warning' => 1
        )
      end
    end

    describe '#latest_assessment_created_at' do
      context 'when assessments exist' do
        let(:latest) { 1.day.ago }

        before :each do
          [latest, 5.days.ago, 10.days.ago, 2.days.ago].each do |created_at|
            create :white_box_assessment, created_at: created_at, docker_command: dc, machine: machine
          end
        end

        it 'returns latest created_at' do
          expect(instance.latest_assessment_created_at.to_i).to eq latest.to_i
        end
      end

      context 'when no assessments exist' do
        it 'returns nil' do
          expect(instance.latest_assessment_created_at).to eq nil
        end
      end
    end
  end

  context 'for orgs' do
    let!(:another_machine) { create :machine, organization: org }
    let(:dc) { create :docker_command, commandable: org }
    let(:instance) { described_class.new(relation: dc) }

    before :each do
      # Create a machine scan to ensure org scans are being summarized correctly
      create :docker_command, commandable: machine
    end

    it 'should have scanned all the machines in the org' do
      create :white_box_assessment, machine: machine, docker_command: dc
      create :white_box_assessment, machine: another_machine, docker_command: dc
      expect(dc.scanned_machine_count).to eq org.machines.count
    end

    describe '#results_summary' do
      it 'totals assessment states' do
        %i[pending_scheduling in_progress complete].each do |state|
          create :white_box_assessment, state: state, docker_command: dc, machine: machine
        end
        %i[complete complete complete].each do |state|
          create :white_box_assessment, state: state, docker_command: dc, machine: another_machine
        end

        machines = instance.results_summary

        m1 = machines.detect { |m| m[:id] == machine.id }
        m2 = machines.detect { |m| m[:id] == another_machine.id }

        m1_expected = { 'pending_scheduling' => 1, 'in_progress' => 1, 'complete' => 1 }
        m2_expected = { 'pending_scheduling' => 0, 'in_progress' => 0, 'complete' => 3 }

        expect(m1[:assessment_states]).to eq m1_expected
        expect(m2[:assessment_states]).to eq m2_expected
      end

      it 'includes the correct machines in the summary' do
        expect(instance.results_summary.map { |m| m[:id] }).to eq org.machines.pluck(:id)
      end

      it 'totals result statuses' do
        a1 = create :white_box_assessment, machine: machine, docker_command: dc
        %i[pass fail warn info error pass error].each do |status|
          create :result, status: status, assessment: a1
        end
        a2 = create :white_box_assessment, machine: another_machine, docker_command: dc
        %i[pass pass].each do |status|
          create :result, status: status, assessment: a2
        end

        machines = instance.results_summary

        m1 = machines.detect { |m| m[:id] == machine.id }
        m2 = machines.detect { |m| m[:id] == another_machine.id }

        m1_expected = { 'passing' => 2, 'erroring' => 2, 'informing' => 1, 'failing' => 1, 'warning' => 1 }
        m2_expected = { 'passing' => 2, 'erroring' => 0, 'informing' => 0, 'failing' => 0, 'warning' => 0 }

        expect(m1[:results_summary]).to eq m1_expected
        expect(m2[:results_summary]).to eq m2_expected
      end
    end

    describe 'latest_assessment_created_at' do
      let(:latest1) { 1.day.ago }
      let(:latest2) { 2.days.ago }

      before :each do
        [latest1, 5.days.ago, 10.days.ago, 2.days.ago].each do |created_at|
          create :white_box_assessment, created_at: created_at, docker_command: dc, machine: machine
        end
        [latest2, 4.days.ago, 11.days.ago, 3.days.ago].each do |created_at|
          create :white_box_assessment, created_at: created_at, docker_command: dc, machine: another_machine
        end
      end

      it 'returns latest created_at' do
        expect(instance.latest_assessment_created_at.to_i).to eq latest1.to_i
      end
    end
  end
end
