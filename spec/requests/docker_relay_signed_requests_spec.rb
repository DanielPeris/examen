# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe 'Docker Relay signed communication', type: :request do
  include NoradControllerTestHelpers

  before :each do
    @keypair = OpenSSL::PKey::RSA.new(4096)
    @relay = create :docker_relay, public_key: Base64.encode64(@keypair.public_key.to_pem).tr("\n", '')
    @payload = { timestamp: Time.now.to_f.to_s, version: { current: '0.0.0' } }
  end

  describe 'heartbeating' do
    def send_heartbeat(relay, payload, signature)
      signed_request(:post, heartbeat_v1_docker_relay_path(relay), payload, signature)
    end

    it 'requires the request to be signed by the agent key' do
      send_heartbeat(@relay, @payload, 'junk')
      expect(response.status).to be 401
      sig = Base64.encode64(@keypair.sign(OpenSSL::Digest::SHA256.new, @payload.to_json)).tr("\n", '')
      send_heartbeat(@relay, @payload, sig)
      expect(response.status).to be 200
      expect(response).to match_response_schema('docker_relay_without_key')
    end

    it 'updates the last_heartbeat timestamp' do
      sig = Base64.encode64(@keypair.sign(OpenSSL::Digest::SHA256.new, @payload.to_json)).tr("\n", '')
      old_heartbeat = @relay.last_heartbeat
      send_heartbeat(@relay, @payload, sig)
      expect(@relay.reload.last_heartbeat).to be > old_heartbeat
    end

    it 'updates the last_reported_version' do
      sig = Base64.encode64(@keypair.sign(OpenSSL::Digest::SHA256.new, @payload.to_json)).tr("\n", '')
      expect(@relay.last_reported_version).to eq nil
      send_heartbeat(@relay, @payload, sig)
      expect(@relay.reload.last_reported_version).to eq '0.0.0'
    end

    it 'turns the Relay online if it was offline' do
      @relay.update_column(:state, 0) # force the Relay offline
      sig = Base64.encode64(@keypair.sign(OpenSSL::Digest::SHA256.new, @payload.to_json)).tr("\n", '')
      send_heartbeat(@relay, @payload, sig)
      expect(@relay.online?).to be(false)
      expect(@relay.reload.online?).to be(true)
    end
  end

  describe 'self-deletion' do
    it 'requires the request to be signed if no API token is provided' do
      payload = { timestamp: Time.now.to_f.to_s }
      sig = Base64.encode64(@keypair.sign(OpenSSL::Digest::SHA256.new, payload.to_json)).tr("\n", '')
      signed_request :delete, v1_docker_relay_path(@relay), payload, 'junk'
      expect(response.status).to be 401
      signed_request :delete, v1_docker_relay_path(@relay), payload, sig
      expect(response.status).to be 204
    end
  end

  describe 'self-retrieval' do
    it 'requires the request to be signed if no API token is provided' do
      payload = { timestamp: Time.now.to_f.to_s }
      sig = Base64.encode64(@keypair.sign(OpenSSL::Digest::SHA256.new, payload.to_json)).tr("\n", '')
      signed_request :get, v1_docker_relay_path(@relay), payload, 'junk'
      expect(response.status).to be 401
      signed_request :get, v1_docker_relay_path(@relay), payload, sig
      expect(response.status).to eq(200)
      expect(response).to match_response_schema('docker_relay_with_key')
    end
  end
end
