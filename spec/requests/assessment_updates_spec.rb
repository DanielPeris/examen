# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe 'Assessment updates', type: :request do
  include NoradControllerTestHelpers

  it 'responds with a 422 if the job takes so long its secret is deleted' do
    assessment = create :assessment
    payload = {
      timestamp: Time.now.to_f.to_s,
      assessment: { state: 'complete' }
    }
    sig = OpenSSL::HMAC.hexdigest(
      'sha256',
      assessment.security_container_secret.secret,
      payload.to_json
    )
    assessment.security_container_secret.update_attributes(secret: nil)
    signed_request :put, v1_assessment_path(assessment), payload, sig

    assert_response 422
    expect(response_body['errors']['base']).to eq(['Secret expired'])
  end
end
