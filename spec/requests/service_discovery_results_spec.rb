# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe 'Service Discovery results', type: :request do
  include NoradControllerTestHelpers

  describe 'require signed requests by the discovery container' do
    before :each do
      @payload = discovery_params.merge(service_params).tap do |h|
        h[:service][:port] = h[:service][:port].to_s
      end
      @discovery = create :service_discovery
      @discovery.start!
    end

    let(:discovery_params) { { via_discovery: 'true', timestamp: Time.now.to_f.to_s } }
    let(:service_params) { { service: attributes_for(:service, machine: nil) } }

    it 'fails if the request is not properly signed' do
      sig = OpenSSL::HMAC.hexdigest('sha256', @discovery.shared_secret, @payload.to_json)
      signed_request :post, v1_machine_services_path(@discovery.machine), @payload, 'junk'
      expect(response.status).to be 401
      signed_request :post, v1_machine_services_path(@discovery.machine), @payload, sig
      expect(response.status).to be 200
    end

    it 'marks the created service as discovered' do
      sig = OpenSSL::HMAC.hexdigest('sha256', @discovery.shared_secret, @payload.to_json)
      signed_request :post, v1_machine_services_path(@discovery.machine), @payload, sig
      service = Service.find(response_body['response']['id'])
      expect(service.discovered).to eq(true)
    end
  end
end
