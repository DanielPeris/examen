###########################################################
# Dockerfile for Norad API
############################################################

# Set the base image to Ubuntu
FROM ruby:2.4.2

# File Authors / Maintainers
LABEL authors="Blake Hitchcock, Brian Manifold, Roger Seagle"

ARG NORAD_API_VERSION_CI
ENV NORAD_API_VERSION=${NORAD_API_VERSION_CI}

# Update the system & add essential libs and programs
RUN apt-get update \
&& apt-get install -y --no-install-recommends \
  build-essential \
  software-properties-common \
  libgmp-dev \
  postgresql-client \
  nodejs \
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
&& apt-get clean autoclean -y \
&& apt-get autoremove -y

RUN wget https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64.deb \
  && dpkg -i dumb-init_*.deb

# Create directory where app will live
ENV APP_PATH /norad/api
RUN mkdir -p $APP_PATH
WORKDIR $APP_PATH

RUN mkdir -p $APP_PATH/tmp && chmod 777 $APP_PATH/tmp \
  && mkdir -p $APP_PATH/log && chmod 777 $APP_PATH/log \
  && touch $APP_PATH/log/lograge_production.log && chmod 777 $APP_PATH/log/lograge_production.log

ENV BUNDLE_PATH /norad/ruby_gems
ENV WORKER_PROCESSES 1
ENV LISTEN_ON 0.0.0.0:8000

COPY Gemfile .
COPY Gemfile.lock .

ARG BUNDLE_DEPLOYMENT="--deployment"
ARG BUNDLE_WITHOUT="development:test:linters:security_auditors:client_test"

# Copying the rails engines
COPY ./engines/client_test_route_helpers ./engines/client_test_route_helpers

# Install Gems
RUN gem install bundler --no-document \
&& bundle install --jobs 5 --retry 5 $BUNDLE_DEPLOYMENT --without $BUNDLE_WITHOUT

# MAKE DKIM PEM --- DO NOT USE THIS IN ACTUAL PRODUCTION
RUN openssl genrsa -out dkim-private.pem 1024 -outform PEM \
  && openssl rsa -in dkim-private.pem -out dkim-public.pem -pubout -outform PEM \
  && mkdir -p $APP_PATH/.dkim \
  && mv dkim-private.pem $APP_PATH/.dkim/ \
  && mv dkim-public.pem $APP_PATH/.dkim/

# Copy the actual rails app to the image
COPY . .

EXPOSE 8000

# Start the Norad API
ENTRYPOINT ["/norad/api/compose-start.sh"]
CMD ["unicorn", "-c", "config/unicorn.rb", "-E", "production"]
