# frozen_string_literal: true

module NoradScanBuilder
  class ScanTarget
    attr_reader :container, :secret_id, :id, :assessment, :assessment_global_id, :docker_command, :machine

    def initialize(container, secret_id, docker_command, machine, _opts = {})
      @container = container
      @secret_id = secret_id
      @docker_command = docker_command
      @machine = machine
      initialize_assessment
    end

    def to_h
      {
        id: id,
        assessment: assessment,
        assessment_global_id: assessment_global_id
      }
    end

    private

    def initialize_assessment
      assessment_obj = docker_command.assessments.create!(assessment_attrs)
      @id = machine.target_address
      @assessment = assessment_obj.results_path
      @assessment_global_id = assessment_obj.to_global_id.to_s
    rescue StandardError => e
      handle_assessment_create_error(assessment_obj, e.message)
      @id = nil
      @assessment = nil
      @assessment_global_id = nil
    end

    def assessment_attrs
      {
        title: container.full_path,
        security_container: container,
        security_container_secret_id: secret_id,
        type: container.whitebox? ? 'WhiteBoxAssessment' : 'BlackBoxAssessment',
        machine: machine
      }
    end

    def handle_assessment_create_error(assessment_obj, message)
      Resque.logger.error "Error creating an assessment for #{machine.name} using test #{container.name}."
      Resque.logger.error "Message: #{message}"
      cancel_assessment(assessment_obj) if assessment_obj&.id
    end

    def cancel_assessment(assessment_obj)
      Resque.logger.error "Canceling assessment #{assessment_obj.identifier}."
      ResultGenerator.new(assessment_obj).create_errored_api_result!
    end
  end
end
