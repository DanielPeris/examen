# frozen_string_literal: true

module NoradScanBuilder
  class OrganizationScanJob
    include ScanJobUtilities

    attr_reader :scans, :secret, :container_full_path, :organization, :dc

    def initialize(org, container, excluded_machines, command)
      @dc = command
      @organization = org
      @secret = SecurityContainerSecret.create!
      @container_full_path = container.full_path
      @scans = create_scans(container, org.machines.where.not(id: excluded_machines))
      @multi_host = container.multi_host
    end

    private

    # XXX Currently multi host scanning is only supported for whole host scans like Qualys
    def create_scans(container, machines)
      [MultiHostScan.new(container, secret.id, dc, organization: organization, machines: machines)]
    end
  end
end
