# frozen_string_literal: true

module NoradScanBuilder
  class ServiceScanTarget < ScanTarget
    attr_reader :service

    def initialize(container, secret_id, docker_command, machine, opts = {})
      @service = opts.fetch(:service)
      super
    end

    private

    def assessment_attrs
      super.merge(service: service)
    end
  end
end
