# frozen_string_literal: true

module NoradScanBuilder
  class MultiHostScanTarget < ScanTarget
    attr_reader :organization

    def initialize(container, secret_id, docker_command, machine, opts = {})
      @organization = opts.fetch(:organization)
      super
    end
  end
end
